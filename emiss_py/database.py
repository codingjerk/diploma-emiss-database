from enum import Enum, auto
from typing import List, Any
import psycopg2


class Type(Enum):
    Text = auto()
    Int64 = auto()
    Decimal = auto()


class Column:
    def __init__(self, name: str, type: Type, is_required: bool) -> None:
        self.name = name
        self.type = type
        self.is_required = is_required

    def __eq__(self, other: Any) -> bool:
        if type(self) != type(other):
            return False

        return all([
            self.name == other.name,
            self.type == other.type,
            self.is_required == other.is_required,
        ])

    def as_sql(self) -> str:
        return " ".join([
            self.name_as_sql(),
            self.type_as_sql(),
            *self.constraints_as_sql(),
        ])

    def name_as_sql(self) -> str:
        return f'"{self.name}"'

    def type_as_sql(self) -> str:
        if self.type == Type.Int64:
            return "BIGINT"
        if self.type == Type.Decimal:
            return "NUMERIC(30, 15)"
        if self.type == Type.Text:
            return "TEXT"

        raise NotImplementedError

    def constraints_as_sql(self) -> List[str]:
        constraints = []
        if self.is_required:
            constraints.append("NOT NULL")

        return constraints


class PostgresDatabase:
    def __init__(
        self,
        host: str,
        port: int,
        database: str,
        schema: str,
        username: str,
        password: str,
    ) -> None:
        self.host = host
        self.port = port
        self.database = database
        self.schema = schema
        self.username = username
        self.password = password

    def connect(self) -> None:
        self.connection = psycopg2.connect(
            f"host={self.host} "
            f"port={self.port} "
            f"dbname={self.database} "
            f"user={self.username} "
            f"password={self.password}"
        )
        self.cursor = self.connection.cursor()

    def commit(self) -> None:
        self.connection.commit()

    def is_table_exists(self, table_name: str) -> bool:
        self.cursor.execute(f"""
            SELECT EXISTS (
                SELECT
                FROM information_schema.tables
                WHERE table_schema = '{self.schema}'
                AND   table_name = '{table_name}'
            );
        """)

        result = self.cursor.fetchall()[0][0]
        assert type(result) is bool, "Expected query result to be boolean"

        return bool(result)

    def create_table(self, table_name: str, columns: List[Column]) -> None:
        columns_as_sql = ",".join(c.as_sql() for c in columns)
        self.cursor.execute(f"""
            CREATE TABLE "{self.schema}"."{table_name}" (
                {columns_as_sql}
            );
        """)

    def insert(
        self,
        table_name: str,
        columns: List[str],
        values: List[List[Any]]
    ) -> None:
        self.cursor.execute(f"""
            INSERT INTO "{self.schema}"."{table_name}" (
                {self.columns_as_sql(columns)}
            ) VALUES
                {self.values_as_sql(values)}
            ;
        """)

    def columns_as_sql(self, columns: List[str]) -> str:
        names = (f'"{c}"' for c in columns)
        return ", ".join(names)

    def values_as_sql(self, values: List[List[Any]]) -> str:
        rows_as_sql = (self.row_as_sql(row) for row in values)
        return ", ".join(rows_as_sql)

    def row_as_sql(self, row: List[Any]) -> str:
        escaped_values = (
            str(v).replace("\\", "\\\\").replace("'", "\\'")
            for v in row
        )
        escaped_values = (
            f"'{v}'"
            for v in escaped_values
        )
        values = ", ".join(escaped_values)
        return f"({values})"

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.commit()
