from decimal import Decimal
from emiss_py.indicator import EmissIndicator
from emiss_py.database import Column, Type, PostgresDatabase
from typing import List, Any, Union


class EmissIndicatorSaver:
    def __init__(self, database: PostgresDatabase) -> None:
        self.database = database

    def save(self, table_name: str, indicator: EmissIndicator) -> None:
        self.ensure_table_exists(table_name, indicator)

        if not indicator.is_empty():
            self.insert_records(table_name, indicator)

    def ensure_table_exists(
        self,
        table_name: str,
        indicator: EmissIndicator
    ) -> None:
        if not self.database.is_table_exists(table_name):
            self.create_table(table_name, indicator)

    def create_table(self, table_name: str, indicator: EmissIndicator) -> None:
        columns = self.get_columns_for_indicator(indicator)
        self.database.create_table(
            table_name=table_name,
            columns=columns,
        )

    def insert_records(
        self,
        table_name: str,
        indicator: EmissIndicator
    ) -> None:
        columns = self.get_indicator_column_names(indicator)
        values = self.get_indicator_records_as_matrix(indicator)
        filtered_values = self.filter_indicator_records(values)

        self.database.insert(
            table_name=table_name,
            columns=columns,
            values=filtered_values,
        )

    def get_columns_for_indicator(
        self,
        indicator: EmissIndicator
    ) -> List[Column]:
        names = self.get_indicator_column_names(indicator)
        types = self.get_indicator_column_types(names)

        return [
            Column(name=name, type=type, is_required=True)
            for name, type in zip(names, types)
        ]

    def get_indicator_column_types(
        self,
        column_names: List[str]
    ) -> List[Type]:
        return [
            self.get_record_type(name)
            for name in column_names
        ]

    def get_indicator_column_names(
        self,
        indicator: EmissIndicator
    ) -> List[str]:
        columns_from_key_names = [
            key_name.replace("s_", "")
            for key_name in self.get_indicator_key_names(indicator)
        ]
        additional_columns = ["value"]
        return columns_from_key_names + additional_columns

    def get_indicator_records_as_matrix(
        self,
        indicator: EmissIndicator
    ) -> List[List[Any]]:
        key_names = self.get_indicator_key_names(indicator)
        records = indicator.as_matrix_with_key_part(key_names)
        self.fix_records_as_matrix_types(records, key_names)

        return records

    def filter_indicator_records(
        self,
        records: List[List[Any]],
    ) -> List[List[Any]]:
        def has_value(record):
            value = record[-1]
            return value != "–"

        return list(filter(has_value, records))

    def fix_records_as_matrix_types(
        self,
        records: List[List[Any]],
        key_names: List[str]
    ) -> None:
        for record in records:
            for key_index, key_name in enumerate(key_names + ["value"]):
                record[key_index] = self.fix_record_value(
                    value=record[key_index],
                    key_name=key_name,
                )

    def get_record_type(self, name: str) -> Type:
        if name == "year":
            return Type.Int64
        if name == "value":
            return Type.Decimal

        return Type.Text

    def fix_record_value(
        self,
        value: str,
        key_name: str
    ) -> Union[int, str, Decimal]:
        if key_name == "year":
            return int(value)
        if key_name == "value" and value != "–":
            return Decimal(value.replace(",", "."))

        return value

    def get_indicator_key_names(self, indicator: EmissIndicator) -> List[str]:
        return [
            key_name
            for key_name in indicator.key_names()
            # NOTE: We are not interested in EI and PERIOD columns
            #       as they are always the same
            # TODO: assert that they are always have same values
            if key_name not in ["EI", "PERIOD"]
        ]
