from typing import Dict, Any, List
from utils import DictUtils


class EmissRecord:
    @classmethod
    def from_sdmx_record(
        cls,
        raw_record: Dict[str, Any],
        code_dictionaries: Dict[str, Dict[str, Any]]
    ):
        key = cls.extract_key_from_sdmx(raw_record, code_dictionaries)
        value = EmissRecord.extract_value_from_sdmx(raw_record)

        return cls.from_key_and_value(key, value)

    @classmethod
    def extract_key_from_sdmx(
        cls,
        raw_record: Dict[str, Any],
        code_dictionaries: Dict[str, Dict[str, Any]]
    ) -> Dict[str, Any]:
        raw_keys = cls.extract_raw_keys_from_sdmx(
            raw_record,
            code_dictionaries,
        )
        raw_attributes = cls.extract_raw_attributes_from_sdmx(raw_record)
        year = cls.extract_year_from_sdmx(raw_record)

        key = DictUtils.merge([raw_keys, raw_attributes, {"year": year}])
        return key

    @classmethod
    def extract_raw_keys_from_sdmx(
        cls,
        raw_record: Dict[str, Any],
        code_dictionaries: Dict[str, Dict[str, Any]]
    ) -> Dict[str, Any]:
        result = {}
        raw_keys: List[Any] = DictUtils.extract_list(
            raw_record,
            ["generic:SeriesKey", "generic:Value"],
        )
        for raw_key in raw_keys:
            concept = raw_key.get("@concept")
            code = raw_key.get("@value")
            value = code_dictionaries[concept][code]
            result[concept] = value

        return result

    @classmethod
    def extract_raw_attributes_from_sdmx(
        cls,
        raw_record: Dict[str, Any]
    ) -> Dict[str, Any]:
        result = {}
        raw_attributes: List[Any] = DictUtils.extract_list(
            raw_record,
            ["generic:Attributes", "generic:Value"],
        )
        for raw_attribute in raw_attributes:
            concept = raw_attribute.get("@concept")
            value = raw_attribute.get("@value")
            result[concept] = value

        return result

    @classmethod
    def extract_year_from_sdmx(cls, raw_record: Dict[str, Any]) -> str:
        result = DictUtils.extract_element(
            raw_record,
            ["generic:Obs", "generic:Time"],
        )
        assert result is not None, "Every record should have a year"

        return result

    @classmethod
    def extract_value_from_sdmx(cls, raw_record: Dict[str, Any]) -> Any:
        return DictUtils.extract_element(
            raw_record,
            ["generic:Obs", "generic:ObsValue", "@value"],
        )

    @classmethod
    def from_key_and_value(cls, key: Dict[str, Any], value: Any):
        return cls(
            key=key,
            value=value,
        )

    def __init__(self, key: Dict[str, Any], value: Any) -> None:
        self.key = key
        self.value = value

    def __eq__(self, other) -> bool:
        return all([
            self.key == other.key,
            self.value == other.value,
        ])

    def get_key_part(self, key_names: List[str]) -> List[Any]:
        return [
            self.key[key_name]
            for key_name in key_names
        ]
