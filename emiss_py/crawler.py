import os
import shutil
from selenium import webdriver
from typing import Any


class EmissCrawler:
    WAIT_TIMEOUT_SECONDS: int = 120
    DOWNLOAD_DIR: str = os.path.abspath("./tmp")

    # NOTE: not intended to be changed
    DOWNLOAD_FILENAME: str = os.path.join(DOWNLOAD_DIR, "data.xml")

    browser: webdriver.Firefox

    @classmethod
    def default(cls) -> Any:
        profile = cls.default_browser_profile()
        browser = webdriver.Firefox(
            profile,
            executable_path="C:\\geckodriver.exe",
        )
        return cls(browser)

    @classmethod
    def default_browser_profile(cls):
        default_preferences = {
            "browser.download.folderList": 2,  # 2 == custom location
            "browser.download.manager.showWhenStarting": False,
            "browser.download.dir": cls.DOWNLOAD_DIR,
            "browser.helperApps.neverAsk.saveToDisk": "text/xml",
        }

        profile = webdriver.FirefoxProfile()
        for name, value in default_preferences.items():
            profile.set_preference(name, value)

        return profile

    def __init__(self, browser: webdriver.Firefox) -> None:
        self.browser = browser

    def quit(self) -> None:
        self.browser.quit()

    def download_sdmx_file(self, url: str) -> str:
        self.browser.get(url)

        self.open_filter_panel()
        self.is_data_splitted_by_years()

        self.initiate_downloading()
        self.wait_for_download_to_end()
        return self.read_downloaded_file()

    def open_filter_panel(self) -> None:
        filter_tab_button = self.browser.find_element_by_css_selector(
            "div#agrid-filters-tab"
        )
        if filter_tab_button is not None:
            pass  # TODO: filter_tab_button.click()

    def is_data_splitted_by_years(self) -> bool:
        filters = self.browser.find_elements_by_css_selector(
            "span.dropdown_trigger"
        )

        for filter in filters:
            if filter.text == "Год":
                return True

        return False

    def initiate_downloading(self) -> None:
        self.clear_download_directory()
        self.select_all_filters()
        self.wait_for_filters_to_apply()
        self.click_on_download_button()

    def clear_download_directory(self) -> None:
        try:
            shutil.rmtree(self.DOWNLOAD_DIR)
        except FileNotFoundError:
            pass

    def wait_for_download_to_end(self) -> None:
        for _ in range(self.WAIT_TIMEOUT_SECONDS):
            if os.path.exists(self.DOWNLOAD_FILENAME):
                return

            self.browser.implicitly_wait(1)

        raise TimeoutError("Got timeout while waiting for download to end")

    def read_downloaded_file(self) -> str:
        with open(self.DOWNLOAD_FILENAME, "r", encoding="utf-8") as file:
            return file.read()

    def click_on_download_button(self) -> None:
        download_button = self.browser.find_element_by_css_selector(
            "button.btn.blue_btn.dropdown_trigger"
        )
        download_button.click()

        download_sdmx_button = self.browser.find_element_by_css_selector(
            "#download_sdmx_file"
        )
        download_sdmx_button.click()

    def wait_for_filters_to_apply(self) -> None:
        loading_indicator = self.browser.find_element_by_css_selector(
            "div.agrid-loader"
        )
        while "display: none" not in loading_indicator.get_attribute("style"):
            self.browser.implicitly_wait(1)

    def select_all_filters(self) -> None:
        self.select_column_filters()
        self.select_year_filter()
        self.click_on_visible_submit_button()

    def select_column_filters(self) -> None:
        column_filters = self.browser.find_elements_by_css_selector(
            "div.k-grid-header-locked span.k-icon.k-filter"
        )
        for column_filter in column_filters:
            self.close_filter_form()
            column_filter.click()
            self.click_on_visible_checkbox()

    def select_year_filter(self) -> None:
        self.close_filter_form()
        first_year_filters = self.browser.find_element_by_css_selector(
            "div.k-grid-header-wrap span.k-icon.k-filter"
        )
        first_year_filters.click()
        self.click_on_visible_checkbox()

    def close_filter_form(self) -> None:
        self.click_on_body()

    def click_on_body(self) -> None:
        body = self.browser.find_element_by_css_selector("body")
        body.click()

    def click_on_visible_checkbox(self) -> None:
        all_checkboxes = self.browser.find_elements_by_css_selector(
            "input.k-check-all + span.sp_checkbox"
        )
        for checkbox in all_checkboxes:
            checkbox_container = checkbox.find_element_by_xpath(
                "../../../../.."
            )
            if self.is_element_visible(checkbox_container):
                checkbox.click()
                return

    def click_on_visible_submit_button(self) -> None:
        all_buttons = self.browser.find_elements_by_css_selector(
            'button[type=submit]'
        )
        for button in all_buttons:
            if button.text == "Фильтровать":
                button.click()
                return

    def is_element_visible(self, element) -> bool:
        style = element.get_attribute("style")
        return "display: none" not in style
