import xmltodict
from typing import List, Any, Dict, Optional
from emiss_py.record import EmissRecord
from utils import DictUtils


class EmissIndicator:
    @classmethod
    def from_sdmx_data(cls, sdmx_data: str):
        parsed_sdmx_data = xmltodict.parse(sdmx_data)
        records = cls.extract_records(parsed_sdmx_data)

        return cls.from_records(records)

    @classmethod
    def from_records(cls, records: List[EmissRecord]):
        return cls(records=records)

    @classmethod
    def extract_records(cls, sdmx_data: Dict[str, Any]) -> List[EmissRecord]:
        code_dictionaries = cls.extract_code_dictionaries(sdmx_data)
        result = []
        raw_records: List[Any] = DictUtils.extract_list(
            sdmx_data,
            ["GenericData", "DataSet", "generic:Series"],
        )
        for raw_record in raw_records:
            record = EmissRecord.from_sdmx_record(
                raw_record,
                code_dictionaries,
            )
            result.append(record)

        return result

    @classmethod
    def extract_code_dictionaries(
        cls,
        sdmx_data: Dict[str, Any]
    ) -> Dict[str, Dict[str, str]]:
        result = {}
        raw_code_lists: List[Any] = DictUtils.extract_list(
            sdmx_data,
            ["GenericData", "CodeLists", "structure:CodeList"],
        )
        for raw_code_list in raw_code_lists:
            concept = raw_code_list["@id"]
            result[concept] = cls.extract_one_code_dictionary(raw_code_list)

        return result

    @classmethod
    def extract_one_code_dictionary(
        cls,
        raw_code_list: Dict[str, Any]
    ) -> Dict[str, str]:
        result = {}
        raw_codes: List[Any] = DictUtils.extract_list(
            raw_code_list,
            ["structure:Code"],
        )
        for raw_code in raw_codes:
            value = cls.extract_text(raw_code, ["structure:Description"])
            assert value is not None, \
                "Unexpected None in code dictionary value"

            code = raw_code["@value"]
            result[code] = value

        return result

    @classmethod
    def extract_text(
        cls,
        node: Dict[str, Any],
        path: List[str]
    ) -> Optional[str]:
        result = DictUtils.extract_element(node, path)

        if result is None:
            return None

        if type(result) is str:
            return result

        return result["#text"]

    def __init__(self, records: List[EmissRecord]) -> None:
        self.records = records

    def is_empty(self) -> bool:
        return len(self.records) == 0

    def key_names(self) -> List[str]:
        return list(self.records[0].key.keys())

    def as_matrix_with_key_part(self, key_names: List[str]) -> List[List[Any]]:
        return [
            [*record.get_key_part(key_names), record.value]
            for record in self.records
        ]
