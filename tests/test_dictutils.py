from utils.dict import DictUtils


# NOTE: there are only tests to cover missing lines in emiss_py tests
def test_extract_list_returns_empty_list_instead_of_none():
    xs = DictUtils.extract_list({"path": None}, ["path"])
    assert xs == []
