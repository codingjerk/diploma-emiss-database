from emiss_py.indicator import EmissIndicator
from emiss_py.record import EmissRecord
from textwrap import dedent


def test_it_extracts_records():
    data = dedent("""\
        <?xml version="1.0" encoding="utf-8"?>
        <GenericData>
            <CodeLists>
                <structure:CodeList id="s_OKFS">
                    <structure:Code value="9">
                        <structure:Description>
                            Всего по формам собственности
                        </structure:Description>
                    </structure:Code>
                </structure:CodeList>
                <structure:CodeList id="s_OKATO">
                    <structure:Code value="643">
                        <structure:Description xml:lang="ru">
                            Российская Федерация
                        </structure:Description>
                    </structure:Code>
                </structure:CodeList>
            </CodeLists>
            <DataSet>
                <generic:Series>
                    <generic:SeriesKey>
                        <generic:Value concept="s_OKFS" value="9"/>
                        <generic:Value concept="s_OKATO" value="643"/>
                    </generic:SeriesKey>
                    <generic:Attributes>
                        <generic:Value concept="EI" value="миллион рублей"/>
                        <generic:Value concept="PERIOD" value="значение показателя за год"/>
                    </generic:Attributes>
                    <generic:Obs>
                        <generic:Time>1993</generic:Time>
                        <generic:ObsValue value="27124,5"/>
                    </generic:Obs>
                </generic:Series>
            </DataSet>
        </GenericData>
    """)

    indicator = EmissIndicator.from_sdmx_data(data)
    assert indicator.records == [
        EmissRecord.from_key_and_value(
            key={
                "s_OKFS": "Всего по формам собственности",
                "s_OKATO": "Российская Федерация",
                "EI": "миллион рублей",
                "PERIOD": "значение показателя за год",
                "year": "1993",
            },
            value="27124,5",
        )
    ]

def test_it_extracts_none_if_there_is_no_text():
    text = EmissIndicator.extract_text({"path": {}}, ["path", "non-existing-subpath"])
    assert text is None
