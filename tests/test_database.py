from emiss_py.database import Column, Type, PostgresDatabase
from unittest.mock import MagicMock
from unittest.mock import patch
import unittest


class TestSqlGeneration(unittest.TestCase):
    def test_column_as_sql(self, *args):
        self.assertEqual(
            Column(name="a", type=Type.Int64, is_required=True).as_sql(),
            '"a" BIGINT NOT NULL',
        )
        self.assertEqual(
            Column(name="b", type=Type.Decimal, is_required=True).as_sql(),
            '"b" NUMERIC(30, 15) NOT NULL',
        )
        self.assertEqual(
            Column(name="c", type=Type.Text, is_required=True).as_sql(),
            '"c" TEXT NOT NULL',
        )


class TestPostgres(unittest.TestCase):
    def create_database(self):
        return PostgresDatabase(
            host="host",
            port=1234,
            database="database",
            schema="schema",
            username="username",
            password="password",
        )

    @patch("psycopg2.connect")
    def test_connect(self, connect):
        database = self.create_database()
        database.connect()

        connection_string = \
            "host=host" \
            " port=1234" \
            " dbname=database" \
            " user=username" \
            " password=password"

        connect.assert_called_once_with(connection_string)
        connection = connect(connection_string)
        connection.cursor.assert_called_once_with()

    @patch("psycopg2.connect")
    def test_commit(self, connect):
        database = self.create_database()
        database.connect()
        database.commit()
        commit = connect().commit

        commit.assert_called_once_with()

    @patch("psycopg2.connect")
    def test_is_table_exists(self, connect):
        cursor = connect().cursor()
        cursor.fetchall = MagicMock(
            return_value=[[False]],
        )

        database = self.create_database()
        database.connect()
        database.is_table_exists("table")

        cursor.execute.assert_called_once()

    @patch("psycopg2.connect")
    def test_create_table(self, connect):
        database = self.create_database()
        database.connect()
        database.create_table("table", [
            Column(name="column", type=Type.Int64, is_required=True),
        ])
        cursor = connect().cursor()

        cursor.execute.assert_called_once()

    @patch("psycopg2.connect")
    def test_insert(self, connect):
        database = self.create_database()
        database.connect()
        database.create_table("table", [
            Column(name="column", type=Type.Int64, is_required=True),
        ])
        cursor = connect().cursor()

        cursor.execute.assert_called_once()

    @patch("psycopg2.connect")
    def test_with(self, connect):
        database = self.create_database()
        with database:
            pass

        connect.assert_called_once()

        connection = connect()
        connection.commit.assert_called_once()

    @patch("psycopg2.connect")
    def test_insert_query(self, connect):
        database = self.create_database()
        database.connect()
        database.insert(
            table_name="some table name",
            columns=["one", "another"],
            values=["1", "2"],
        )

        cursor = connect().cursor()
        query = cursor.execute.call_args[0][0]

        assert "INSERT" in query.upper()
        assert '"schema"."some table name"' in query
