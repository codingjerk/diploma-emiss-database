from decimal import Decimal
from emiss_py.indicator_saver import EmissIndicatorSaver
from emiss_py.indicator import EmissIndicator
from emiss_py.record import EmissRecord
from emiss_py.database import Column, Type
from unittest.mock import MagicMock
import unittest


class TestWorkWithDatabase(unittest.TestCase):
    def create_mock_database(self):
        return MagicMock()

    def test_it_creates_table_for_indicator_if_it_does_not_exist(self):
        mock_database = self.create_mock_database()
        mock_database.is_table_exists = MagicMock(return_value=False)
        indicator = EmissIndicator.from_records([
            EmissRecord.from_key_and_value(
                key={
                    "s_OKFS": "OKFS Test",
                    "s_OKATO": "OKATO Test",
                    "EI": "should be ignored",
                    "PERIOD": "значение показателя за год",
                    "year": "1993",
                },
                value="100,1",
            )
        ])
        table_name = "test_indicator"

        saver = EmissIndicatorSaver(mock_database)
        saver.save(table_name, indicator)

        mock_database.is_table_exists.assert_called_once_with(table_name)
        mock_database.create_table.assert_called_once_with(table_name=table_name, columns=[
            Column(name="OKFS", type=Type.Text, is_required=True),
            Column(name="OKATO", type=Type.Text, is_required=True),
            Column(name="year", type=Type.Int64, is_required=True),
            Column(name="value", type=Type.Decimal, is_required=True),
        ])

    def test_it_inserts_records(self):
        mock_database = self.create_mock_database()
        indicator = EmissIndicator.from_records([
            EmissRecord.from_key_and_value(
                key={
                    "s_OKFS": "OKFS Test",
                    "s_OKATO": "OKATO Test",
                    "EI": "should be ignored",
                    "PERIOD": "значение показателя за год",
                    "year": "1993",
                },
                value="100,1",
            )
        ])
        table_name = "test_indicator"

        saver = EmissIndicatorSaver(mock_database)
        saver.save(table_name, indicator)

        mock_database.insert.assert_called_once_with(
            table_name=table_name,
            columns=["OKFS", "OKATO", "year", "value"],
            values=[["OKFS Test", "OKATO Test", 1993, Decimal("100.1")]]
        )

    def test_it_skips_records_without_value(self):
        mock_database = self.create_mock_database()
        indicator = EmissIndicator.from_records([
            EmissRecord.from_key_and_value(key={"key": "ok_key"}, value="1"),
            EmissRecord.from_key_and_value(key={"key": "not_ok_key"}, value="–"),
            EmissRecord.from_key_and_value(key={"key": "another_ok_key"}, value="3"),
        ])
        table_name = "any_name"

        saver = EmissIndicatorSaver(mock_database)
        saver.save(table_name, indicator)
        mock_database.insert.assert_called_once_with(
            table_name=table_name,
            columns=["key", "value"],
            values=[
                ["ok_key", 1],
                ["another_ok_key", 3],
            ],
        )
