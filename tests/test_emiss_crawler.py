from emiss_py.crawler import EmissCrawler
from unittest.mock import MagicMock
from unittest.mock import patch
import unittest


class TestCrawling(unittest.TestCase):
    def create_mock_browser(self):
        mock_browser = MagicMock()
        element = mock_browser.find_element_by_css_selector(
            "div.agrid-loader"
        )
        element.get_attribute = MagicMock(
            return_value="display: none",
        )

        return mock_browser

    def create_mock_open(self, data):
        mock_open = MagicMock()
        file_mock = mock_open.__enter__()
        file_mock.read = MagicMock(return_value=data)

        return mock_open

    def crawl(self, browser=None, open=None, rmtree=None):
        if browser is None:
            browser = self.create_mock_browser()

        if open is None:
            open = self.create_mock_open("fake data")

        if rmtree is None:
            rmtree = MagicMock()

        @patch("os.path.exists", return_value=True)
        @patch("shutil.rmtree", rmtree)
        @patch("emiss_py.crawler.open", return_value=open)
        def crawl(*args):
            crawler = EmissCrawler(browser=browser)
            crawler.download_sdmx_file("fake url")

        crawl()

    @patch("os.path.exists", return_value=True)
    @patch("shutil.rmtree")
    def test_it_returns_sdmx_data(self, *args):
        expected_sdmx_data = "<fake>sdmx data</fake>"
        mock_browser = self.create_mock_browser()
        mock_open = self.create_mock_open(expected_sdmx_data)

        with patch("emiss_py.crawler.open", return_value=mock_open):
            crawler = EmissCrawler(browser=mock_browser)
            sdmx_data = crawler.download_sdmx_file("fake url")

        self.assertEqual(sdmx_data, expected_sdmx_data)

    def test_it_closes_browser_at_the_end(self):
        mock_browser = self.create_mock_browser()
        crawler = EmissCrawler(browser=mock_browser)
        crawler.quit()

        mock_browser.quit.assert_called()

    def test_it_clears_temporary_directory_at_the_start(self):
        mock_rmtree = MagicMock()
        self.crawl(rmtree=mock_rmtree)

        mock_rmtree.assert_called()

    def test_it_should_ignore_if_download_directory_does_not_exist(self, *args):
        mock_rmtree = MagicMock(side_effect=FileNotFoundError("fake error"))
        self.crawl(rmtree=mock_rmtree)

    def test_it_should_open_filter_panel(self):
        mock_browser = self.create_mock_browser()
        mock_filter_element = MagicMock()
        self.crawl(browser=mock_browser)

        mock_browser.find_element_by_css_selector.assert_any_call("div#agrid-filters-tab")
        mock_filter_button = mock_browser.find_element_by_css_selector("div#agrid-filters-tab")
        mock_filter_button.click.assert_called()

    def test_it_should_check_for_year_filter(self):
        mock_browser = self.create_mock_browser()
        mock_filter_element = MagicMock()
        mock_browser.find_elements_by_css_selector = MagicMock(return_value=[mock_filter_element])
        self.crawl(browser=mock_browser)

        mock_browser.find_elements_by_css_selector.assert_any_call("span.dropdown_trigger")
        mock_filter_element.text.__eq__.assert_any_call("Год")
