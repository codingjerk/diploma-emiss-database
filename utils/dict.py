from typing import List, Dict, TypeVar, Any, Optional


A = TypeVar("A")
B = TypeVar("B")


class DictUtils:
    @classmethod
    def extract_list(cls, node: Dict[str, Any], path: List[str]) -> List[Any]:
        xs = cls.extract_element(node, path)
        if xs is None:
            return []

        if type(xs) is not list:
            return [xs]

        return list(xs)

    @classmethod
    def extract_element(cls, node: Any, path: List[str]) -> Optional[Any]:
        if node is None:
            return None

        if len(path) == 1:
            return node.get(path[0])

        [current_path_element, *rest_path_elements] = path
        next_node = node.get(current_path_element)

        return cls.extract_element(next_node, rest_path_elements)

    @classmethod
    def merge(cls, dictionaries: List[Dict[A, B]]) -> Dict[A, B]:
        result = {}
        for dictionary in dictionaries:
            result.update(dictionary)

        return result
