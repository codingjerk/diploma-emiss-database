all: test lint typecheck coverage

test:
	@echo [ === TEST === ]
	@PYTHONPATH=. python3 -m pytest --quiet

lint:
	@echo [ === LINT === ]
	@PYTHONPATH=. python3 -m pycodestyle . --exclude venv,tests

typecheck:
	@echo [ === TYPECHECK === ]
	@PYTHONPATH=. python3 -m mypy --pretty --no-error-summary --ignore-missing-imports download_emiss_to_database.py

coverage:
	@echo [ === COVERAGE === ]
	@PYTHONPATH=. python3 -m pytest --cov=. --cov-fail-under=88 --cov-report=term-missing:skip-covered --quiet

quality:
	@echo [ === QUALITY === ]
	@radon mi download_emiss_to_database.py emiss_py/*.py
	@radon cc download_emiss_to_database.py emiss_py/*.py

watch:
	@echo [ === WATCH === ]
	@PYTHONPATH=. python3 -m pytest_watch --onpass "make --silent lint && make --silent typecheck && make --silent coverage" -- --quiet
