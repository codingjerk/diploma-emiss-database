import os
import pandas
import matplotlib.pyplot as plt
from sqlalchemy import create_engine
from sklearn.linear_model import LinearRegression


database_connection_string = \
    "postgresql://emiss:" \
    f"{os.environ['EMISS_DB_PASSWORD']}" \
    "@116.203.226.190/emiss"

plt.interactive(False)


def forecast_region(dataframe, year_from, year_to, test):
    X = dataframe.index.values.reshape(-1, 1)  # X = [2010, 2011, 2012]
    Y = dataframe.iloc[:, 0].values.reshape(-1, 1)  # Y = [1, 2, 3]
    Xf = [[year] for year in range(year_from, year_to + 1)]  # Xf = [year_from, ..., year_to]

    linear_regressor = LinearRegression()
    linear_regressor.fit(X, Y)
    Yf = linear_regressor.predict(Xf)

    dataframe_forecast = pandas.DataFrame(
        data={
            "value": [y[0] for y in Yf],
        },
        index=[x[0] for x in Xf],
    )

    dataframe_full = pandas.concat([dataframe, dataframe_forecast])
    dataframe_full = dataframe_full.loc[~dataframe_full.index.duplicated(keep='first')].sort_index()
    dataframe_full = dataframe_full.loc[year_from:year_to]

    if test:
        coef = linear_regressor.coef_
        const = linear_regressor.intercept_
        r2 = linear_regressor.score(X, Y)

        print("====$", coef)
        print("====*", const)
        print("====#", r2)
        # Yf2 = dataframe_full.values
        # plt.scatter(X, Y)
        # plt.plot(Xf, Yf2, color="blue")
        # plt.plot(Xf, Yf, color="red")
        # plt.show()

    return dataframe_full


def forecast_okato_dataframe(dataframe, year_from, year_to, test):
    result = None

    for region in list(dataframe.index.unique(level=0)):
        # print("Region:", region)
        print("====%", region)
        region_dataframe = dataframe.loc[region]
        forecasted_dataframe = forecast_region(region_dataframe, year_from, year_to, test)

        forecasted_dataframe_with_region_level = pandas.concat([forecasted_dataframe], keys=[region], names=["okato", "year"])
        if result is None:
            result = forecasted_dataframe_with_region_level
        else:
            result = pandas.concat([result, forecasted_dataframe_with_region_level])

    return result


def extract_forecast_load_okato(table_name, year_from, year_to, clip, test):
    print("====^", table_name)
    dataframe = pandas.read_sql(f"SELECT * FROM normalized.{table_name}", database_connection_string, index_col=["okato", "year"])
    dataframe = forecast_okato_dataframe(dataframe, year_from, year_to, test)

    if clip:
        dataframe = dataframe.clip(lower=0)

    if test:
        pass
        # dataframe.drop("РОССИЙСКАЯ ФЕДЕРАЦИЯ")
        # dataframe.unstack(level=0).plot()
        # plt.show()
    else:
        engine = create_engine(database_connection_string)
        dataframe.to_sql(table_name, engine, schema="forecasted")


extract_forecast_load_okato("pensioners", 2000, 2025+1, clip=True, test=True)
extract_forecast_load_okato("decease_unemployable", 2000-1, 2025, clip=True, test=True)
extract_forecast_load_okato("decease_employable", 2000-1, 2025, clip=True, test=True)
extract_forecast_load_okato("retriement_employable", 2000-1, 2025, clip=True, test=True)
extract_forecast_load_okato("ex_students_private", 2000, 2025, clip=True, test=True)
extract_forecast_load_okato("ex_students_gov", 2000, 2025, clip=True, test=True)
extract_forecast_load_okato("ex_students_employed_total", 2000, 2025, clip=True, test=True)
extract_forecast_load_okato("migrants", 2000, 2025, clip=False, test=True)
extract_forecast_load_okato("new_bachelors_private", 2000-4, 2025, clip=True, test=True)
extract_forecast_load_okato("new_bachelors_gov", 2000-4, 2025, clip=True, test=True)
extract_forecast_load_okato("new_masters_gov", 2000-4, 2025, clip=True, test=True)
extract_forecast_load_okato("ex_students_employed_vpo", 2000, 2025, clip=True, test=True)
extract_forecast_load_okato("ex_students_conscripted", 2000-1, 2025, clip=True, test=True)
extract_forecast_load_okato("vrp_category_a", 2000, 2025, clip=True, test=True)
extract_forecast_load_okato("average_employees_category_a", 2000, 2025, clip=True, test=True)
extract_forecast_load_okato("production_index_category_b", 2000, 2025, clip=True, test=True)
extract_forecast_load_okato("average_employees_category_b", 2000, 2025, clip=True, test=True)
extract_forecast_load_okato("investments_category_c", 2000, 2025, clip=True, test=True)
extract_forecast_load_okato("unemployed_mot", 2000-1, 2025, clip=True, test=True)
extract_forecast_load_okato("employed", 2000-1, 2025, clip=True, test=True)
extract_forecast_load_okato("average_employees", 2000-1, 2025, clip=True, test=True)
extract_forecast_load_okato("population", 2000, 2025, clip=True, test=True)
