import os
import pandas
import numpy
import matplotlib.pyplot as plt


def draw(dataframe):
    filter = numpy.in1d(dataframe.index.get_level_values(0), ['Г. МОСКВА', 'АМУРСКАЯ ОБЛАСТЬ', 'ИРКУТСКАЯ ОБЛАСТЬ'])
    dataframe = dataframe[filter]
    dataframe.unstack(level=0).plot()
    plt.show()


database_connection_string = \
    "postgresql://emiss:" \
    f"{os.environ['EMISS_DB_PASSWORD']}" \
    "@116.203.226.190/emiss"


PARAMS = {
    "F_investments_multiplier": 1 / 70000,
    "F_population_multiplier": 10 / 100,
    "F_multiplier": 0.05,
}


def get_t0(dataframe, year):
    return dataframe.swaplevel().loc[year]


# === Выбытие с рынка труда ===
pensioners_t = pandas.read_sql("SELECT * FROM forecasted.pensioners", database_connection_string, index_col=["okato", "year"])
pensioners_t_p1 = pandas.read_sql("SELECT okato, (year - 1) AS year, value FROM forecasted.pensioners", database_connection_string, index_col=["okato", "year"])
decease_unemployable_t_m1 = pandas.read_sql("SELECT okato, (year + 1) AS year, value FROM forecasted.decease_unemployable", database_connection_string, index_col=["okato", "year"])

retriement_t = (pensioners_t_p1 - (pensioners_t - decease_unemployable_t_m1).clip(lower=0)).clip(lower=0)

decease_employable_t = pandas.read_sql("SELECT * FROM forecasted.decease_employable", database_connection_string, index_col=["okato", "year"])
retriement_employable_t = pandas.read_sql("SELECT * FROM forecasted.retriement_employable", database_connection_string, index_col=["okato", "year"])
retriement_total_t = retriement_t + decease_employable_t + retriement_employable_t


# === Общее ===
ex_students_private_t = pandas.read_sql("SELECT * FROM forecasted.ex_students_private", database_connection_string, index_col=["okato", "year"])
ex_students_gov_t = pandas.read_sql("SELECT * FROM forecasted.ex_students_gov", database_connection_string, index_col=["okato", "year"])
ex_students_t = ex_students_gov_t + ex_students_private_t


# === Занятые (воспроизводство) ===
ex_students_employed_total_t = pandas.read_sql("SELECT * FROM forecasted.ex_students_employed_total", database_connection_string, index_col=["okato", "year"])
migrants_t = pandas.read_sql("SELECT * FROM forecasted.migrants", database_connection_string, index_col=["okato", "year"])
employed_total_t = ex_students_employed_total_t + migrants_t


# === Безработные (воспроизводство) ===
new_bachelors_private_t_m4 = pandas.read_sql("SELECT okato, (year + 4) AS year, value FROM forecasted.new_bachelors_private", database_connection_string, index_col=["okato", "year"])
new_bachelors_gov_t_m4 = pandas.read_sql("SELECT okato, (year + 4) AS year, value FROM forecasted.new_bachelors_gov", database_connection_string, index_col=["okato", "year"])
new_masters_gov_t_m2 = pandas.read_sql("SELECT okato, (year + 2) AS year, value FROM forecasted.new_masters_gov", database_connection_string, index_col=["okato", "year"])
new_masters_gov_t_m4 = pandas.read_sql("SELECT okato, (year + 4) AS year, value FROM forecasted.new_masters_gov", database_connection_string, index_col=["okato", "year"])
should_ex_students_t = new_bachelors_private_t_m4 + new_bachelors_gov_t_m4 + new_masters_gov_t_m2 + new_masters_gov_t_m4

excluded_students_t = (should_ex_students_t - ex_students_t) / pandas.concat([should_ex_students_t, ex_students_t]).max(level=[0, 1]) * 100
excluded_students_t = excluded_students_t.clip(lower=0)

ex_students_employed_vpo_t = pandas.read_sql("SELECT * FROM forecasted.ex_students_employed_vpo", database_connection_string, index_col=["okato", "year"])
new_masters_gov_t = pandas.read_sql("SELECT * FROM forecasted.new_masters_gov", database_connection_string, index_col=["okato", "year"])
ex_students_conscripted_t = pandas.read_sql("SELECT * FROM forecasted.ex_students_conscripted", database_connection_string, index_col=["okato", "year"])

ex_students_unemployed_t = ((ex_students_employed_vpo_t + new_masters_gov_t + ex_students_conscripted_t) - ex_students_t).clip(lower=0)
ex_students_conscripted_t_m1 = pandas.read_sql("SELECT okato, (year + 1) AS year, value FROM forecasted.ex_students_conscripted", database_connection_string, index_col=["okato", "year"])

unemployed_total_t = (excluded_students_t + ex_students_unemployed_t + ex_students_conscripted_t_m1).clip(lower=0)


# === Необходимая численность работников ===
# == ВЭДы A ==
vrp_category_a_t = pandas.read_sql("SELECT * FROM forecasted.vrp_category_a", database_connection_string, index_col=["okato", "year"])
vrp_category_a_t0 = get_t0(vrp_category_a_t, 2015)

average_employees_category_a_t_m1 = pandas.read_sql("SELECT okato, (year + 1) AS year, value FROM forecasted.average_employees_category_a", database_connection_string, index_col=["okato", "year"])
average_employees_category_a_t = pandas.read_sql("SELECT * FROM forecasted.average_employees_category_a", database_connection_string, index_col=["okato", "year"])
average_employees_category_a_t0 = get_t0(average_employees_category_a_t, 2015)

demand_a_t = vrp_category_a_t / vrp_category_a_t0 * average_employees_category_a_t_m1

# == ВЭДы B ==
production_index_category_b_t = pandas.read_sql("SELECT * FROM forecasted.production_index_category_b", database_connection_string, index_col=["okato", "year"])

average_employees_category_b_t_m1 = pandas.read_sql("SELECT okato, (year + 1) AS year, value FROM forecasted.average_employees_category_b", database_connection_string, index_col=["okato", "year"])
average_employees_category_b_t = pandas.read_sql("SELECT * FROM forecasted.average_employees_category_b", database_connection_string, index_col=["okato", "year"])
average_employees_category_b_t0 = get_t0(average_employees_category_b_t, 2015)

demand_b_t = production_index_category_b_t * average_employees_category_b_t_m1

# == ВЭДы C ==
investments_category_c_t = pandas.read_sql("SELECT * FROM forecasted.investments_category_c", database_connection_string, index_col=["okato", "year"])
population_t = pandas.read_sql("SELECT * FROM forecasted.population", database_connection_string, index_col=["okato", "year"])

demand_c_t = PARAMS["F_multiplier"] * (investments_category_c_t * PARAMS["F_investments_multiplier"] + population_t * PARAMS["F_population_multiplier"])
demand_c_t = demand_c_t.clip(lower=0)


# === Дополнительная кадровая потребность ===
unemployed_mot_t_m1 = pandas.read_sql("SELECT okato, (year + 1) AS year, value FROM forecasted.unemployed_mot", database_connection_string, index_col=["okato", "year"])
employed_t_m1 = pandas.read_sql("SELECT okato, (year + 1) AS year, value FROM forecasted.employed", database_connection_string, index_col=["okato", "year"])
retriement_employable_t_m1 = pandas.read_sql("SELECT okato, (year + 1) AS year, value FROM forecasted.retriement_employable", database_connection_string, index_col=["okato", "year"])

# NOTE: can be checked
unemployed_mot_t = unemployed_total_t + (unemployed_mot_t_m1 - employed_t_m1).clip(lower=0) + (retriement_employable_t_m1 - retriement_t).clip(lower=0)

average_employees_t_m1 = pandas.read_sql("SELECT okato, (year + 1) AS year, value FROM forecasted.average_employees", database_connection_string, index_col=["okato", "year"])

# NOTE: can be checked
average_employees_t = (average_employees_t_m1 - retriement_total_t) + employed_total_t

demand_t = demand_a_t + demand_b_t + demand_c_t
additional_demand_t = demand_t - average_employees_t


# === Проверочные показатели ===
retraining_t = pandas.read_sql("SELECT * FROM normalized.retraining", database_connection_string, index_col=["okato", "year"])
needed_employees_declared_t = pandas.read_sql("SELECT * FROM normalized.needed_employees_declared", database_connection_string, index_col=["year"])
needed_employees_to_fill_t = pandas.read_sql("SELECT * FROM normalized.needed_employees_to_fill", database_connection_string, index_col=["okato", "okved", "year"])


# === Export ===
cummulative_t = pandas.concat([
    demand_a_t,
    demand_b_t,
    demand_c_t,
    demand_t,
    retriement_total_t,
    employed_total_t,
    unemployed_total_t,
    additional_demand_t,
    unemployed_mot_t,
    retraining_t,
    average_employees_t,
    population_t,
    vrp_category_a_t,
    production_index_category_b_t,
], axis=1).query("year >= 2000 and year <= 2025")
cummulative_t.columns = [
    "Необходимая среднечисленность (A)",
    "Необходимая среднечисленность (B)",
    "Необходимая среднечисленность (C)",
    "Необходимая среднечисленность",
    "Выбытие",
    "Занятые (воспроизводство)",
    "Безработные (воспроизводство)",
    # Потребность на замену,
    "Дополнительная потребность",
    "Запас трудовых ресурсов",
    "Переподготовленные",
    "Среднесписочная численность работников по полному кругу организаций",
    "Предположительная численность населения",
    "ВРП (для ВЕДов А)",
    "Индекс производства (для ВЕДов Б)",
]

cummulative_t.to_csv("Дополнительная потребность.csv", decimal=",", sep=";", encoding="windows-1251")


# === Error ===
test_forecast_t = (additional_demand_t + retriement_total_t).groupby(level=1).sum()
test_survey_t = needed_employees_declared_t + needed_employees_to_fill_t.groupby(level=2).sum()

error_t = pandas.concat([
    test_forecast_t,
    test_survey_t,
], axis=1)
error_t.columns = ["Вычисленные значения", "Ожидаемые значения"]
error_t.to_csv("Проверка расчётов.csv", decimal=",", sep=";", encoding="windows-1251")
