from emiss_py.crawler import EmissCrawler
from emiss_py.indicator import EmissIndicator
from emiss_py.database import PostgresDatabase
from emiss_py.indicator_saver import EmissIndicatorSaver

from typing import List, Tuple
import os


# Structure: (TABLE_NAME, INDICATOR_URL)
indicator_list: List[Tuple[(str, str)]] = [
    # Checked:
    # ("indicator_31556", "https://fedstat.ru/indicator/31556"),
    # ("indicator_33379", "https://fedstat.ru/indicator/33379"),
    # ("indicator_33414", "https://fedstat.ru/indicator/33414"),
    # ("indicator_36243", "https://fedstat.ru/indicator/36243"),
    # ("indicator_36250", "https://fedstat.ru/indicator/36250"),
    # ("indicator_36654", "https://fedstat.ru/indicator/36654"),
    # ("indicator_36660", "https://fedstat.ru/indicator/36660"),
    # ("indicator_36727", "https://fedstat.ru/indicator/36727"),
    # ("indicator_37613", "https://fedstat.ru/indicator/37613"),
    # ("indicator_42960", "https://fedstat.ru/indicator/42960"),
    # ("indicator_44261", "https://fedstat.ru/indicator/44261"),
    # ("indicator_44263", "https://fedstat.ru/indicator/44263"),
    # ("indicator_44264", "https://fedstat.ru/indicator/44264"),
    # ("indicator_44265", "https://fedstat.ru/indicator/44265"),
    # ("indicator_44266", "https://fedstat.ru/indicator/44266"),
    # ("indicator_44267", "https://fedstat.ru/indicator/44267"),
    # ("indicator_44268", "https://fedstat.ru/indicator/44268"),
    # ("indicator_44269", "https://fedstat.ru/indicator/44269"),
    # ("indicator_44270", "https://fedstat.ru/indicator/44270"),
    # ("indicator_44271", "https://fedstat.ru/indicator/44271"),
    # ("indicator_44272", "https://fedstat.ru/indicator/44272"),
    # ("indicator_44274", "https://fedstat.ru/indicator/44274"),
    # ("indicator_53375", "https://fedstat.ru/indicator/53375"),
    # ("indicator_53376", "https://fedstat.ru/indicator/53376"),
    # ("indicator_53377", "https://fedstat.ru/indicator/53377"),
    # ("indicator_53378", "https://fedstat.ru/indicator/53378"),
    # ("indicator_53379", "https://fedstat.ru/indicator/53379"),
    # ("indicator_53396", "https://fedstat.ru/indicator/53396"),
    # ("indicator_57316", "https://fedstat.ru/indicator/57316"),
    # ("indicator_57509", "https://fedstat.ru/indicator/57509"),
    # ("indicator_57512", "https://fedstat.ru/indicator/57512"),
    # ("indicator_58264", "https://fedstat.ru/indicator/58264"),
    # ("indicator_59644", "https://fedstat.ru/indicator/59644"),
    # ("indicator_59645", "https://fedstat.ru/indicator/59645"),
    # ("indicator_59719", "https://fedstat.ru/indicator/59719"),
    # ("indicator_59926", "https://fedstat.ru/indicator/59926"),

    # Manual loaded:
    # ("indicator_43141", "https://fedstat.ru/indicator/43141"),
    # ("indicator_55096", "https://fedstat.ru/indicator/55096"),
    # ("indicator_40608", "https://fedstat.ru/indicator/40608"),
    # ("indicator_58614", "https://fedstat.ru/indicator/58614"),
    # ("indicator_58613", "https://fedstat.ru/indicator/58613"),
    # ("indicator_58148", "https://fedstat.ru/indicator/58148"),
    # ("indicator_33453", "https://fedstat.ru/indicator/33453"),
    # ("indicator_57886", "https://fedstat.ru/indicator/57886"),
    # ("indicator_33555", "https://fedstat.ru/indicator/33555"),
]


def main() -> None:
    crawler = EmissCrawler.default()
    database = PostgresDatabase(
        host="db.codingjerk.dev",
        port=5432,
        database="emiss",
        schema="raw",
        username="emiss",
        password=os.environ["EMISS_DB_PASSWORD"],
    )
    database.connect()
    saver = EmissIndicatorSaver(database=database)

    for (table_name, url) in indicator_list:
        load_one_indicator(
            crawler,
            saver,
            table_name=table_name,
            url=url,
        )
        database.commit()

    for file_name in os.listdir("local-indicators"):
        if not file_name.endswith(".xml"):
            continue

        table_name = get_base_name(file_name)
        load_one_local_indicator(
            saver,
            table_name=table_name,
            file_name=os.path.join("local-indicators", file_name),
        )
        database.commit()

    database.commit()
    crawler.quit()


def get_base_name(full_path: str) -> str:
    file_name = os.path.split(full_path)[-1]
    base_name = os.path.splitext(file_name)[0]

    return base_name


def load_one_indicator(
    crawler: EmissCrawler,
    saver: EmissIndicatorSaver,
    table_name: str,
    url: str
) -> None:
    print(f"Processing indicator for table {table_name}")
    sdmx_data = crawler.download_sdmx_file(url)
    indicator = EmissIndicator.from_sdmx_data(sdmx_data)
    saver.save(table_name, indicator)


def load_one_local_indicator(
    saver: EmissIndicatorSaver,
    table_name: str,
    file_name: str,
) -> None:
    print(f"Processing local indicator for table {table_name}")
    with open(file_name, "r", encoding="utf-8") as file:
        sdmx_data = file.read()

    indicator = EmissIndicator.from_sdmx_data(sdmx_data)
    saver.save(table_name, indicator)


if __name__ == "__main__":
    main()
