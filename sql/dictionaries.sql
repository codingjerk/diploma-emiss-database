SELECT okato, COUNT(indicator), ARRAY_AGG(okato), ARRAY_AGG(DISTINCT type), ARRAY_AGG(indicator) FROM (
    SELECT DISTINCT "OKATO" AS okato, 'indicator_31556' AS indicator, 'upper' AS type FROM raw.indicator_31556
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_33379' AS indicator, 'upper' AS type FROM raw.indicator_33379
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_33414' AS indicator, 'upper' AS type FROM raw.indicator_33414
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_33453' AS indicator, 'upper' AS type FROM raw.indicator_33453
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_33465' AS indicator, 'upper' AS type FROM raw.indicator_33465
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_36654' AS indicator, 'upper' AS type FROM raw.indicator_36654
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_37613' AS indicator, 'upper' AS type FROM raw.indicator_37613
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_43048_2000' AS indicator, 'upper' AS type FROM raw.indicator_43048_2000
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_43048_2005' AS indicator, 'upper' AS type FROM raw.indicator_43048_2005
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_43048_2011' AS indicator, 'upper' AS type FROM raw.indicator_43048_2011
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_57316' AS indicator, 'upper' AS type FROM raw.indicator_57316
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_57806' AS indicator, 'upper' AS type FROM raw.indicator_57806
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_57886' AS indicator, 'upper' AS type FROM raw.indicator_57886
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_58090' AS indicator, 'upper' AS type FROM raw.indicator_58090
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_58145_1' AS indicator, 'upper' AS type FROM raw.indicator_58145_1
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_58145_2' AS indicator, 'upper' AS type FROM raw.indicator_58145_2
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_58145_3' AS indicator, 'upper' AS type FROM raw.indicator_58145_3
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_58148_1' AS indicator, 'upper' AS type FROM raw.indicator_58148_1
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_58148_2' AS indicator, 'upper' AS type FROM raw.indicator_58148_2
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_58148_3' AS indicator, 'upper' AS type FROM raw.indicator_58148_3
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_58614' AS indicator, 'upper' AS type FROM raw.indicator_58614
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_58699' AS indicator, 'upper' AS type FROM raw.indicator_58699
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_58775' AS indicator, 'upper' AS type FROM raw.indicator_58775
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_59086' AS indicator, 'upper' AS type FROM raw.indicator_59086
UNION ALL
    SELECT DISTINCT "OKATO" AS okato, 'indicator_59448' AS indicator, 'upper' AS type FROM raw.indicator_59448
UNION ALL
    SELECT DISTINCT "mОКАТО" AS okato, 'indicator_44263' AS indicator, 'M' AS type FROM raw.indicator_44263
UNION ALL
    SELECT DISTINCT "mОКАТО" AS okato, 'indicator_44266' AS indicator, 'M' AS type FROM raw.indicator_44266
UNION ALL
    SELECT DISTINCT "mОКАТО" AS okato, 'indicator_44267' AS indicator, 'M' AS type FROM raw.indicator_44267
UNION ALL
    SELECT DISTINCT "mОКАТО" AS okato, 'indicator_44268' AS indicator, 'M' AS type FROM raw.indicator_44268
UNION ALL
    SELECT DISTINCT "mОКАТО" AS okato, 'indicator_44271' AS indicator, 'M' AS type FROM raw.indicator_44271
UNION ALL
    SELECT DISTINCT "mОКАТО" AS okato, 'indicator_44274' AS indicator, 'M' AS type FROM raw.indicator_44274
UNION ALL
    SELECT DISTINCT "mОКАТО" AS okato, 'indicator_58264' AS indicator, 'M' AS type FROM raw.indicator_58264
UNION ALL
    SELECT DISTINCT okato AS okato, 'indicator_40534' AS indicator, 'lower' AS type FROM raw.indicator_40534
UNION ALL
    SELECT DISTINCT okato AS okato, 'indicator_40608' AS indicator, 'lower' AS type FROM raw.indicator_40608
UNION ALL
    SELECT DISTINCT okato AS okato, 'indicator_43007' AS indicator, 'lower' AS type FROM raw.indicator_43007
) okato
GROUP BY okato;

SELECT normalized.normalize_okved(okved), COUNT(indicator), ARRAY_AGG(indicator) FROM (
    SELECT DISTINCT UPPER("OKVED") AS okved, 'indicator_33379' AS indicator FROM raw.indicator_33379
UNION ALL
    SELECT DISTINCT UPPER("OKVED") AS okved, 'indicator_33453' AS indicator FROM raw.indicator_33453
UNION ALL
    SELECT DISTINCT UPPER("OKVED") AS okved, 'indicator_33465' AS indicator FROM raw.indicator_33465
UNION ALL
    SELECT DISTINCT UPPER(okved) AS okved, 'indicator_40534' AS indicator FROM raw.indicator_40534
UNION ALL
    SELECT DISTINCT UPPER(okved) AS okved, 'indicator_40608' AS indicator FROM raw.indicator_40608
UNION ALL
    SELECT DISTINCT UPPER(okved) AS okved, 'indicator_43007' AS indicator FROM raw.indicator_43007
UNION ALL
    SELECT DISTINCT UPPER("OKVED") AS okved, 'indicator_43048_2000' AS indicator FROM raw.indicator_43048_2000
UNION ALL
    SELECT DISTINCT UPPER("OKVED") AS okved, 'indicator_43048_2005' AS indicator FROM raw.indicator_43048_2005
UNION ALL
    SELECT DISTINCT UPPER("OKVED") AS okved, 'indicator_43048_2011' AS indicator FROM raw.indicator_43048_2011
UNION ALL
    SELECT DISTINCT UPPER("OKVED") AS okved, 'indicator_57886' AS indicator FROM raw.indicator_57886
) okved
GROUP BY normalized.normalize_okved(okved)
ORDER BY COUNT(indicator) DESC;

SELECT okved2, COUNT(indicator), ARRAY_AGG(indicator) FROM (
    SELECT DISTINCT "OKVED2" AS okved2, 'indicator_57806' AS indicator FROM raw.indicator_57806
UNION ALL
    SELECT DISTINCT "OKVED2" AS okved2, 'indicator_58090' AS indicator FROM raw.indicator_58090
UNION ALL
    SELECT DISTINCT "OKVED2" AS okved2, 'indicator_58145_1' AS indicator FROM raw.indicator_58145_1
UNION ALL
    SELECT DISTINCT "OKVED2" AS okved2, 'indicator_58145_2' AS indicator FROM raw.indicator_58145_2
UNION ALL
    SELECT DISTINCT "OKVED2" AS okved2, 'indicator_58145_3' AS indicator FROM raw.indicator_58145_3
UNION ALL
    SELECT DISTINCT "OKVED2" AS okved2, 'indicator_58148_1' AS indicator FROM raw.indicator_58148_1
UNION ALL
    SELECT DISTINCT "OKVED2" AS okved2, 'indicator_58148_2' AS indicator FROM raw.indicator_58148_2
UNION ALL
    SELECT DISTINCT "OKVED2" AS okved2, 'indicator_58148_3' AS indicator FROM raw.indicator_58148_3
UNION ALL
    SELECT DISTINCT "OKVED2" AS okved2, 'indicator_58699' AS indicator FROM raw.indicator_58699
UNION ALL
    SELECT DISTINCT "OKVED2" AS okved2, 'indicator_59086' AS indicator FROM raw.indicator_59086
UNION ALL
    SELECT DISTINCT "OKVED2" AS okved2, 'indicator_59448' AS indicator FROM raw.indicator_59448
) okved2
where okved2 ilike '%всего%'
GROUP BY okved2
-- GROUP BY normalized.normalize_okved2(okved2)
ORDER BY COUNT(indicator) DESC;