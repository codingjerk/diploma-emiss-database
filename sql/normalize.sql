CREATE SCHEMA normalized;
CREATE SCHEMA forecasted;


CREATE OR REPLACE FUNCTION normalized.normalize_okved(okved TEXT) RETURNS TEXT AS
$$
BEGIN
    RETURN CASE
        WHEN UPPER(okved) IN (
            'РАЗДЕЛ А СЕЛЬСКОЕ ХОЗЯЙСТВО, ОХОТА И ЛЕСНОЕ ХОЗЯЙСТВО'
        )
        THEN 'РАЗДЕЛ A СЕЛЬСКОЕ ХОЗЯЙСТВО, ОХОТА И ЛЕСНОЕ ХОЗЯЙСТВО'

        WHEN UPPER(okved) IN (
            'РАЗДЕЛ B РЫБОЛОВСТВО, РЫБОВОДСТВО',
            'РАЗДЕЛ В РЫБОЛОВСТВО, РЫБОВОДСТВО',
            'РЫБОЛОВСТВО'
        )
        THEN 'РАЗДЕЛ B РЫБОЛОВСТВО, РЫБОВОДСТВО'

        WHEN UPPER(okved) IN (
            'РАЗДЕЛ С ДОБЫЧА ПОЛЕЗНЫХ ИСКОПАЕМЫХ'
        )
        THEN 'РАЗДЕЛ C ДОБЫЧА ПОЛЕЗНЫХ ИСКОПАЕМЫХ'

        WHEN UPPER(okved) IN (
            'РАЗДЕЛ D ОБРАБАТЫВАЮЩИЕ ПРОИЗВОДСТВА'
        )
        THEN 'РАЗДЕЛ D ОБРАБАТЫВАЮЩИЕ ПРОИЗВОДСТВА'

        WHEN UPPER(okved) IN (
            'РАЗДЕЛ E ПРОИЗВОДСТВО И РАСПРЕДЕЛЕНИЕ ЭЛЕКТРОЭНЕРГИИ, ГАЗА И ВОДЫ',
            'РАЗДЕЛ Е ПРОИЗВОДСТВО И РАСПРЕДЕЛЕНИЕ ЭЛЕКТРОЭНЕРГИИ,  ГАЗА И ВОДЫ'
        )
        THEN 'РАЗДЕЛ E ПРОИЗВОДСТВО И РАСПРЕДЕЛЕНИЕ ЭЛЕКТРОЭНЕРГИИ, ГАЗА И ВОДЫ'

        WHEN UPPER(okved) IN (
            'РАЗДЕЛ F   СТРОИТЕЛЬСТВО',
            'РАЗДЕЛ F СТРОИТЕЛЬСТВО'
        )
        THEN 'РАЗДЕЛ F СТРОИТЕЛЬСТВО'

        WHEN UPPER(okved) IN (
            'РАЗДЕЛ G ОПТОВАЯ И РОЗНИЧНАЯ ТОРГОВЛЯ; РЕМОНТ АВТОТРАНСПОРТНЫХ СРЕДСТВ, МОТОЦИКЛОВ, БЫТОВЫХ ИЗДЕЛИЙ И ПРЕДМЕТОВ ЛИЧНОГО ПОЛЬЗОВАНИЯ',
            'РАЗДЕЛ G ОПТОВАЯ И РОЗНИЧНАЯ ТОРГОВЛЯ; РЕМОНТ  АВТОТРАНСПОРТНЫХ СРЕДСТВ, МОТОЦИКЛОВ, БЫТОВЫХ ИЗДЕЛИЙ И  ПРЕДМЕТОВ ЛИЧНОГО ПОЛЬЗОВАНИЯ'
        )
        THEN 'РАЗДЕЛ G ОПТОВАЯ И РОЗНИЧНАЯ ТОРГОВЛЯ; РЕМОНТ АВТОТРАНСПОРТНЫХ СРЕДСТВ, МОТОЦИКЛОВ, БЫТОВЫХ ИЗДЕЛИЙ И ПРЕДМЕТОВ ЛИЧНОГО ПОЛЬЗОВАНИЯ'

        WHEN UPPER(okved) IN (
            'РАЗДЕЛ H   ГОСТИНИЦЫ И РЕСТОРАНЫ',
            'РАЗДЕЛ Н ГОСТИНИЦЫ И РЕСТОРАНЫ'
        )
        THEN 'РАЗДЕЛ H ГОСТИНИЦЫ И РЕСТОРАНЫ'

        WHEN UPPER(okved) IN (
            'РАЗДЕЛ I ТРАНСПОРТ И СВЯЗЬ',
            'РАЗДЕЛ I1 ТРАНСПОРТ'
        )
        THEN 'РАЗДЕЛ I ТРАНСПОРТ И СВЯЗЬ'

        WHEN UPPER(okved) IN (
            'РАЗДЕЛ J   ФИНАНСОВАЯ ДЕЯТЕЛЬНОСТЬ',
            'РАЗДЕЛ J ФИНАНСОВАЯ ДЕЯТЕЛЬНОСТЬ'
        )
        THEN 'РАЗДЕЛ J ФИНАНСОВАЯ ДЕЯТЕЛЬНОСТЬ'

        WHEN UPPER(okved) IN (
            'РАЗДЕЛ K ОПЕРАЦИИ С НЕДВИЖИМЫМ ИМУЩЕСТВОМ, АРЕНДА И ПРЕДОСТАВЛЕНИЕ УСЛУГ',
            'РАЗДЕЛ K ОПЕРАЦИИ С НЕДВИЖИМЫМ ИМУЩЕСТВОМ, АРЕНДА И  ПРЕДОСТАВЛЕНИЕ УСЛУГ'
        )
        THEN 'РАЗДЕЛ K ОПЕРАЦИИ С НЕДВИЖИМЫМ ИМУЩЕСТВОМ, АРЕНДА И ПРЕДОСТАВЛЕНИЕ УСЛУГ'

        WHEN UPPER(okved) IN (
            'РАЗДЕЛ L ГОСУДАРСТВЕННОЕ УПРАВЛЕНИЕ И ОБЕСПЕЧЕНИЕ ВОЕННОЙ БЕЗОПАСНОСТИ; СОЦИАЛЬНОЕ СТРАХОВАНИЕ',
            'РАЗДЕЛ L ГОСУДАРСТВЕННОЕ УПРАВЛЕНИЕ И ОБЕСПЕЧЕНИЕ  ВОЕННОЙ БЕЗОПАСНОСТИ;  СОЦИАЛЬНОЕ ОБЕСПЕЧЕНИЕ'
        )
        THEN 'РАЗДЕЛ L ГОСУДАРСТВЕННОЕ УПРАВЛЕНИЕ И ОБЕСПЕЧЕНИЕ ВОЕННОЙ БЕЗОПАСНОСТИ; СОЦИАЛЬНОЕ СТРАХОВАНИЕ'

        WHEN UPPER(okved) IN (
            'РАЗДЕЛ M   ОБРАЗОВАНИЕ',
            'РАЗДЕЛ M ОБРАЗОВАНИЕ'
        )
        THEN 'РАЗДЕЛ M ОБРАЗОВАНИЕ'

        WHEN UPPER(okved) IN (
            'РАЗДЕЛ N   ЗДРАВООХРАНЕНИЕ И ПРЕДОСТАВЛЕНИЕ СОЦИАЛЬНЫХ  УСЛУГ',
            'РАЗДЕЛ N ЗДРАВООХРАНЕНИЕ И ПРЕДОСТАВЛЕНИЕ СОЦИАЛЬНЫХ  УСЛУГ'
        )
        THEN 'РАЗДЕЛ N ЗДРАВООХРАНЕНИЕ И ПРЕДОСТАВЛЕНИЕ СОЦИАЛЬНЫХ  УСЛУГ'

        WHEN UPPER(okved) IN (
            'РАЗДЕЛ O   ПРЕДОСТАВЛЕНИЕ ПРОЧИХ КОММУНАЛЬНЫХ,  СОЦИАЛЬНЫХ И ПЕРСОНАЛЬНЫХ УСЛУГ',
            'РАЗДЕЛ O ПРЕДОСТАВЛЕНИЕ ПРОЧИХ КОММУНАЛЬНЫХ,  СОЦИАЛЬНЫХ И ПЕРСОНАЛЬНЫХ УСЛУГ'
        )
        THEN 'РАЗДЕЛ O ПРЕДОСТАВЛЕНИЕ ПРОЧИХ КОММУНАЛЬНЫХ,  СОЦИАЛЬНЫХ И ПЕРСОНАЛЬНЫХ УСЛУГ'

        WHEN UPPER(okved) IN (
            'РАЗДЕЛ Р ДЕЯТЕЛЬНОСТЬ ДОМАШНИХ ХОЗЯЙСТВ',
            'РАЗДЕЛ Р ДЕЯТЕЛЬНОСТЬ ДОМАШНИХ  ХОЗЯЙСТВ'
        )
        THEN 'РАЗДЕЛ P ДЕЯТЕЛЬНОСТЬ ДОМАШНИХ ХОЗЯЙСТВ'

        WHEN UPPER(okved) IN (
            'РАЗДЕЛ Q ДЕЯТЕЛЬНОСТЬ ЭКСТЕРРИТОРИАЛЬНЫХ ОРГАНИЗАЦИЙ'
        )
        THEN 'РАЗДЕЛ Q ДЕЯТЕЛЬНОСТЬ ЭКСТЕРРИТОРИАЛЬНЫХ ОРГАНИЗАЦИЙ'

        WHEN UPPER(okved) IN (
            'ВСЕГО'
        )
        THEN NULL -- 'ВСЕГО'
    END;
END
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION normalized.normalize_okved2(okved2 TEXT) RETURNS TEXT AS
$$
BEGIN
    -- NOTE: case matters here
    RETURN CASE
        -- A
        WHEN okved2 IN (
            'СЕЛЬСКОЕ, ЛЕСНОЕ ХОЗЯЙСТВО, ОХОТА, РЫБОЛОВСТВО И РЫБОВОДСТВО'
        )
        THEN 'СЕЛЬСКОЕ, ЛЕСНОЕ ХОЗЯЙСТВО, ОХОТА, РЫБОЛОВСТВО И РЫБОВОДСТВО'

        -- B
        WHEN okved2 IN (
            'ДОБЫЧА ПОЛЕЗНЫХ ИСКОПАЕМЫХ'
        )
        THEN 'ДОБЫЧА ПОЛЕЗНЫХ ИСКОПАЕМЫХ'

        -- C
        WHEN okved2 IN (
            'ОБРАБАТЫВАЮЩИЕ ПРОИЗВОДСТВА'
        )
        THEN 'ОБРАБАТЫВАЮЩИЕ ПРОИЗВОДСТВА'

        -- D
        WHEN okved2 IN (
            'ОБЕСПЕЧЕНИЕ ЭЛЕКТРИЧЕСКОЙ ЭНЕРГИЕЙ, ГАЗОМ И ПАРОМ; КОНДИЦИОНИРОВАНИЕ ВОЗДУХА'
        )
        THEN 'ОБЕСПЕЧЕНИЕ ЭЛЕКТРИЧЕСКОЙ ЭНЕРГИЕЙ, ГАЗОМ И ПАРОМ; КОНДИЦИОНИРОВАНИЕ ВОЗДУХА'

        -- E
        WHEN okved2 IN (
            'ВОДОСНАБЖЕНИЕ; ВОДООТВЕДЕНИЕ, ОРГАНИЗАЦИЯ СБОРА И УТИЛИЗАЦИИ ОТХОДОВ, ДЕЯТЕЛЬНОСТЬ ПО ЛИКВИДАЦИИ ЗАГРЯЗНЕНИЙ'
        )
        THEN 'ВОДОСНАБЖЕНИЕ; ВОДООТВЕДЕНИЕ, ОРГАНИЗАЦИЯ СБОРА И УТИЛИЗАЦИИ ОТХОДОВ, ДЕЯТЕЛЬНОСТЬ ПО ЛИКВИДАЦИИ ЗАГРЯЗНЕНИЙ'

        -- F
        WHEN okved2 IN (
            'СТРОИТЕЛЬСТВО'
        )
        THEN 'СТРОИТЕЛЬСТВО'

        -- G
        WHEN okved2 IN (
            'ТОРГОВЛЯ ОПТОВАЯ И РОЗНИЧНАЯ; РЕМОНТ АВТОТРАНСПОРТНЫХ СРЕДСТВ И МОТОЦИКЛОВ'
        )
        THEN 'ТОРГОВЛЯ ОПТОВАЯ И РОЗНИЧНАЯ; РЕМОНТ АВТОТРАНСПОРТНЫХ СРЕДСТВ И МОТОЦИКЛОВ'

        -- H
        WHEN okved2 IN (
            'ТРАНСПОРТИРОВКА И ХРАНЕНИЕ'
        )
        THEN 'ТРАНСПОРТИРОВКА И ХРАНЕНИЕ'

        -- I
        WHEN okved2 IN (
            'ДЕЯТЕЛЬНОСТЬ ГОСТИНИЦ И ПРЕДПРИЯТИЙ ОБЩЕСТВЕННОГО ПИТАНИЯ'
        )
        THEN 'ДЕЯТЕЛЬНОСТЬ ГОСТИНИЦ И ПРЕДПРИЯТИЙ ОБЩЕСТВЕННОГО ПИТАНИЯ'

        -- J
        WHEN okved2 IN (
            'ДЕЯТЕЛЬНОСТЬ В ОБЛАСТИ ИНФОРМАЦИИ И СВЯЗИ'
        )
        THEN 'ДЕЯТЕЛЬНОСТЬ В ОБЛАСТИ ИНФОРМАЦИИ И СВЯЗИ'

        -- K
        WHEN okved2 IN (
            'ДЕЯТЕЛЬНОСТЬ ФИНАНСОВАЯ И СТРАХОВАЯ'
        )
        THEN 'ДЕЯТЕЛЬНОСТЬ ФИНАНСОВАЯ И СТРАХОВАЯ'

        -- L
        WHEN okved2 IN (
            'ДЕЯТЕЛЬНОСТЬ ПО ОПЕРАЦИЯМ С НЕДВИЖИМЫМ ИМУЩЕСТВОМ'
        )
        THEN 'ДЕЯТЕЛЬНОСТЬ ПО ОПЕРАЦИЯМ С НЕДВИЖИМЫМ ИМУЩЕСТВОМ'

        -- M
        WHEN okved2 IN (
            'ДЕЯТЕЛЬНОСТЬ ПРОФЕССИОНАЛЬНАЯ, НАУЧНАЯ И ТЕХНИЧЕСКАЯ'
        )
        THEN 'ДЕЯТЕЛЬНОСТЬ ПРОФЕССИОНАЛЬНАЯ, НАУЧНАЯ И ТЕХНИЧЕСКАЯ'

        -- N
        WHEN okved2 IN (
            'ДЕЯТЕЛЬНОСТЬ АДМИНИСТРАТИВНАЯ И СОПУТСТВУЮЩИЕ ДОПОЛНИТЕЛЬНЫЕ УСЛУГИ'
        )
        THEN 'ДЕЯТЕЛЬНОСТЬ АДМИНИСТРАТИВНАЯ И СОПУТСТВУЮЩИЕ ДОПОЛНИТЕЛЬНЫЕ УСЛУГИ'

        -- O
        WHEN okved2 IN (
            'ГОСУДАРСТВЕННОЕ УПРАВЛЕНИЕ И ОБЕСПЕЧЕНИЕ ВОЕННОЙ БЕЗОПАСНОСТИ; СОЦИАЛЬНОЕ ОБЕСПЕЧЕНИЕ'
        )
        THEN 'ГОСУДАРСТВЕННОЕ УПРАВЛЕНИЕ И ОБЕСПЕЧЕНИЕ ВОЕННОЙ БЕЗОПАСНОСТИ; СОЦИАЛЬНОЕ ОБЕСПЕЧЕНИЕ'

        -- P
        WHEN okved2 IN (
            'ОБРАЗОВАНИЕ'
        )
        THEN 'ОБРАЗОВАНИЕ'

        -- Q
        WHEN okved2 IN (
            'ДЕЯТЕЛЬНОСТЬ В ОБЛАСТИ ЗДРАВООХРАНЕНИЯ И СОЦИАЛЬНЫХ УСЛУГ'
        )
        THEN 'ДЕЯТЕЛЬНОСТЬ В ОБЛАСТИ ЗДРАВООХРАНЕНИЯ И СОЦИАЛЬНЫХ УСЛУГ'

        -- R
        WHEN okved2 IN (
            'ДЕЯТЕЛЬНОСТЬ В ОБЛАСТИ КУЛЬТУРЫ, СПОРТА, ОРГАНИЗАЦИИ ДОСУГА И РАЗВЛЕЧЕНИЙ'
        )
        THEN 'ДЕЯТЕЛЬНОСТЬ В ОБЛАСТИ КУЛЬТУРЫ, СПОРТА, ОРГАНИЗАЦИИ ДОСУГА И РАЗВЛЕЧЕНИЙ'

        -- S
        WHEN okved2 IN (
            'ПРЕДОСТАВЛЕНИЕ ПРОЧИХ ВИДОВ УСЛУГ'
        )
        THEN 'ПРЕДОСТАВЛЕНИЕ ПРОЧИХ ВИДОВ УСЛУГ'

        -- T
        WHEN okved2 IN (
            'ДЕЯТЕЛЬНОСТЬ ДОМАШНИХ ХОЗЯЙСТВ КАК РАБОТОДАТЕЛЕЙ; НЕДИФФЕРЕНЦИРОВАННАЯ ДЕЯТЕЛЬНОСТЬ ЧАСТНЫХ ДОМАШНИХ ХОЗЯЙСТВ ПО ПРОИЗВОДСТВУ ТОВАРОВ И ОКАЗАНИЮ УСЛУГ ДЛЯ СОБСТВЕННОГО ПОТРЕБЛЕНИЯ'
        )
        THEN 'ДЕЯТЕЛЬНОСТЬ ДОМАШНИХ ХОЗЯЙСТВ КАК РАБОТОДАТЕЛЕЙ; НЕДИФФЕРЕНЦИРОВАННАЯ ДЕЯТЕЛЬНОСТЬ ЧАСТНЫХ ДОМАШНИХ ХОЗЯЙСТВ ПО ПРОИЗВОДСТВУ ТОВАРОВ И ОКАЗАНИЮ УСЛУГ ДЛЯ СОБСТВЕННОГО ПОТРЕБЛЕНИЯ'

        -- U
        WHEN okved2 IN (
            'ДЕЯТЕЛЬНОСТЬ ЭКСТЕРРИТОРИАЛЬНЫХ ОРГАНИЗАЦИЙ И ОРГАНОВ'
        )
        THEN 'ДЕЯТЕЛЬНОСТЬ ЭКСТЕРРИТОРИАЛЬНЫХ ОРГАНИЗАЦИЙ И ОРГАНОВ'

        -- Total
        WHEN okved2 IN (
            'Всего по обследуемым видам экономической деятельности'
        )
        THEN NULL -- 'Всего по обследуемым видам экономической деятельности'
    END;
END
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION normalized.normalize_okato(okato TEXT) RETURNS TEXT AS
$$
BEGIN
    RETURN CASE
        WHEN UPPER(okato) IN (
            'АЛТАЙСКИЙ КРАЙ'
        )
        THEN 'АЛТАЙСКИЙ КРАЙ'

        WHEN UPPER(okato) IN (
            'АМУРСКАЯ ОБЛАСТЬ'
        )
        THEN 'АМУРСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'АРХАНГЕЛЬСКАЯ ОБЛАСТЬ'
            -- 'АРХАНГЕЛЬСКАЯ ОБЛАСТЬ (БЕЗ АО)',
            -- 'АРХАНГЕЛЬСКАЯ ОБЛАСТЬ (КРОМЕ НЕНЕЦКОГО АВТОНОМНОГО ОКРУГА)'
        )
        THEN 'АРХАНГЕЛЬСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'АСТРАХАНСКАЯ ОБЛАСТЬ'
        )
        THEN 'АСТРАХАНСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'БЕЛГОРОДСКАЯ ОБЛАСТЬ'
        )
        THEN 'БЕЛГОРОДСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'БРЯНСКАЯ ОБЛАСТЬ'
        )
        THEN 'БРЯНСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'ВЛАДИМИРСКАЯ ОБЛАСТЬ'
        )
        THEN 'ВЛАДИМИРСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'ВОЛГОГРАДСКАЯ ОБЛАСТЬ'
        )
        THEN 'ВОЛГОГРАДСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'ВОЛОГОДСКАЯ ОБЛАСТЬ'
        )
        THEN 'ВОЛОГОДСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'ВОРОНЕЖСКАЯ ОБЛАСТЬ'
        )
        THEN 'ВОРОНЕЖСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'ЕВРЕЙСКАЯ АВТОНОМНАЯ ОБЛАСТЬ'
        )
        THEN 'ЕВРЕЙСКАЯ АВТОНОМНАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'ЗАБАЙКАЛЬСКИЙ КРАЙ'
        )
        THEN 'ЗАБАЙКАЛЬСКИЙ КРАЙ'

        WHEN UPPER(okato) IN (
            'ИВАНОВСКАЯ ОБЛАСТЬ'
        )
        THEN 'ИВАНОВСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'ИРКУТСКАЯ ОБЛАСТЬ'
        )
        THEN 'ИРКУТСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'КАБАРДИНО-БАЛКАРСКАЯ РЕСПУБЛИКА'
        )
        THEN 'КАБАРДИНО-БАЛКАРСКАЯ РЕСПУБЛИКА'

        WHEN UPPER(okato) IN (
            'КАЛИНИНГРАДСКАЯ ОБЛАСТЬ'
        )
        THEN 'КАЛИНИНГРАДСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'КАЛУЖСКАЯ ОБЛАСТЬ'
        )
        THEN 'КАЛУЖСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'КАМЧАТСКИЙ КРАЙ'
        )
        THEN 'КАМЧАТСКИЙ КРАЙ'

        WHEN UPPER(okato) IN (
            'КАРАЧАЕВО-ЧЕРКЕССКАЯ РЕСПУБЛИКА'
        )
        THEN 'КАРАЧАЕВО-ЧЕРКЕССКАЯ РЕСПУБЛИКА'

        WHEN UPPER(okato) IN (
            'КИРОВСКАЯ ОБЛАСТЬ'
        )
        THEN 'КИРОВСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'КОСТРОМСКАЯ ОБЛАСТЬ'
        )
        THEN 'КОСТРОМСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'КРАСНОДАРСКИЙ КРАЙ'
        )
        THEN 'КРАСНОДАРСКИЙ КРАЙ'

        WHEN UPPER(okato) IN (
            'КРАСНОЯРСКИЙ КРАЙ'
        )
        THEN 'КРАСНОЯРСКИЙ КРАЙ'

        WHEN UPPER(okato) IN (
            'КУРГАНСКАЯ ОБЛАСТЬ'
        )
        THEN 'КУРГАНСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'КУРСКАЯ ОБЛАСТЬ'
        )
        THEN 'КУРСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'ЛЕНИНГРАДСКАЯ ОБЛАСТЬ'
        )
        THEN 'ЛЕНИНГРАДСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'ЛИПЕЦКАЯ ОБЛАСТЬ'
        )
        THEN 'ЛИПЕЦКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'МАГАДАНСКАЯ ОБЛАСТЬ'
        )
        THEN 'МАГАДАНСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'МОСКОВСКАЯ ОБЛАСТЬ'
        )
        THEN 'МОСКОВСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'МУРМАНСКАЯ ОБЛАСТЬ'
        )
        THEN 'МУРМАНСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'НИЖЕГОРОДСКАЯ ОБЛАСТЬ'
        )
        THEN 'НИЖЕГОРОДСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'НОВГОРОДСКАЯ ОБЛАСТЬ'
        )
        THEN 'НОВГОРОДСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'НОВОСИБИРСКАЯ ОБЛАСТЬ'
        )
        THEN 'НОВОСИБИРСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'ОМСКАЯ ОБЛАСТЬ'
        )
        THEN 'ОМСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'ОРЕНБУРГСКАЯ ОБЛАСТЬ'
        )
        THEN 'ОРЕНБУРГСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'ОРЛОВСКАЯ ОБЛАСТЬ'
        )
        THEN 'ОРЛОВСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'ПЕНЗЕНСКАЯ ОБЛАСТЬ'
        )
        THEN 'ПЕНЗЕНСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'ПЕРМСКИЙ КРАЙ'
        )
        THEN 'ПЕРМСКИЙ КРАЙ'

        WHEN UPPER(okato) IN (
            'ПРИМОРСКИЙ КРАЙ'
        )
        THEN 'ПРИМОРСКИЙ КРАЙ'

        WHEN UPPER(okato) IN (
            'ПСКОВСКАЯ ОБЛАСТЬ'
        )
        THEN 'ПСКОВСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'РЕСПУБЛИКА АЛТАЙ'
        )
        THEN 'РЕСПУБЛИКА АЛТАЙ'

        WHEN UPPER(okato) IN (
            'РЕСПУБЛИКА БАШКОРТОСТАН'
        )
        THEN 'РЕСПУБЛИКА БАШКОРТОСТАН'

        WHEN UPPER(okato) IN (
            'РЕСПУБЛИКА БУРЯТИЯ'
        )
        THEN 'РЕСПУБЛИКА БУРЯТИЯ'

        WHEN UPPER(okato) IN (
            'РЕСПУБЛИКА ДАГЕСТАН'
        )
        THEN 'РЕСПУБЛИКА ДАГЕСТАН'

        WHEN UPPER(okato) IN (
            'РЕСПУБЛИКА КАЛМЫКИЯ'
        )
        THEN 'РЕСПУБЛИКА КАЛМЫКИЯ'

        WHEN UPPER(okato) IN (
            'РЕСПУБЛИКА КАРЕЛИЯ'
        )
        THEN 'РЕСПУБЛИКА КАРЕЛИЯ'

        WHEN UPPER(okato) IN (
            'РЕСПУБЛИКА КОМИ'
        )
        THEN 'РЕСПУБЛИКА КОМИ'

        WHEN UPPER(okato) IN (
            'РЕСПУБЛИКА МАРИЙ ЭЛ'
        )
        THEN 'РЕСПУБЛИКА МАРИЙ ЭЛ'

        WHEN UPPER(okato) IN (
            'РЕСПУБЛИКА МОРДОВИЯ'
        )
        THEN 'РЕСПУБЛИКА МОРДОВИЯ'

        WHEN UPPER(okato) IN (
            'РЕСПУБЛИКА САХА (ЯКУТИЯ)'
        )
        THEN 'РЕСПУБЛИКА САХА (ЯКУТИЯ)'

        WHEN UPPER(okato) IN (
            'РЕСПУБЛИКА ТЫВА'
        )
        THEN 'РЕСПУБЛИКА ТЫВА'

        WHEN UPPER(okato) IN (
            'РЕСПУБЛИКА ХАКАСИЯ'
        )
        THEN 'РЕСПУБЛИКА ХАКАСИЯ'

        WHEN UPPER(okato) IN (
            'РОСТОВСКАЯ ОБЛАСТЬ'
        )
        THEN 'РОСТОВСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'РЯЗАНСКАЯ ОБЛАСТЬ'
        )
        THEN 'РЯЗАНСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'САМАРСКАЯ ОБЛАСТЬ'
        )
        THEN 'САМАРСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'САРАТОВСКАЯ ОБЛАСТЬ'
        )
        THEN 'САРАТОВСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'САХАЛИНСКАЯ ОБЛАСТЬ'
        )
        THEN 'САХАЛИНСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'СВЕРДЛОВСКАЯ ОБЛАСТЬ'
        )
        THEN 'СВЕРДЛОВСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'СМОЛЕНСКАЯ ОБЛАСТЬ'
        )
        THEN 'СМОЛЕНСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'СТАВРОПОЛЬСКИЙ КРАЙ'
        )
        THEN 'СТАВРОПОЛЬСКИЙ КРАЙ'

        WHEN UPPER(okato) IN (
            'ТАМБОВСКАЯ ОБЛАСТЬ'
        )
        THEN 'ТАМБОВСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'ТВЕРСКАЯ ОБЛАСТЬ'
        )
        THEN 'ТВЕРСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'ТОМСКАЯ ОБЛАСТЬ'
        )
        THEN 'ТОМСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'ТУЛЬСКАЯ ОБЛАСТЬ'
        )
        THEN 'ТУЛЬСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'ТЮМЕНСКАЯ ОБЛАСТЬ'
            -- 'ТЮМЕНСКАЯ ОБЛАСТЬ (КРОМЕ ХАНТЫ-МАНСИЙСКОГО АВТОНОМНОГО ОКРУГА - ЮГРЫ И ЯМАЛО-НЕНЕЦКОГО АВТОНОМНОГО ОКРУГА)',
            -- 'ТЮМЕНСКАЯ ОБЛАСТЬ (БЕЗ АО)',
            -- 'ТЮМЕНСКАЯ ОБЛАСТЬ (КРОМЕ ХАНТЫ-МАНСИЙСКОГО АВТОНОМНОГО ОКРУГА-ЮГРЫ И ЯМАЛО-НЕНЕЦКОГО АВТОНОМНОГО ОКРУГА)'
        )
        THEN 'ТЮМЕНСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'УДМУРТСКАЯ РЕСПУБЛИКА'
        )
        THEN 'УДМУРТСКАЯ РЕСПУБЛИКА'

        WHEN UPPER(okato) IN (
            'УЛЬЯНОВСКАЯ ОБЛАСТЬ'
        )
        THEN 'УЛЬЯНОВСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'ХАБАРОВСКИЙ КРАЙ'
        )
        THEN 'ХАБАРОВСКИЙ КРАЙ'

        WHEN UPPER(okato) IN (
            'ЧЕЛЯБИНСКАЯ ОБЛАСТЬ'
        )
        THEN 'ЧЕЛЯБИНСКАЯ ОБЛАСТЬ'

        WHEN UPPER(okato) IN (
            'ЧУКОТСКИЙ АВТОНОМНЫЙ ОКРУГ'
        )
        THEN 'ЧУКОТСКИЙ АВТОНОМНЫЙ ОКРУГ'

        WHEN UPPER(okato) IN (
            'ЯРОСЛАВСКАЯ ОБЛАСТЬ'
        )
        THEN 'ЯРОСЛАВСКАЯ ОБЛАСТЬ'

        -- Города

        WHEN UPPER(okato) IN (
            'Г.САНКТ-ПЕТЕРБУРГ',
            'Г. САНКТ-ПЕТЕРБУРГ',
            'ГОРОД САНКТ-ПЕТЕРБУРГ ГОРОД ФЕДЕРАЛЬНОГО ЗНАЧЕНИЯ'
        )
        THEN 'Г. САНКТ-ПЕТЕРБУРГ'

        WHEN UPPER(okato) IN (
            'Г. СЕВАСТОПОЛЬ',
            'ГОРОД ФЕДЕРАЛЬНОГО ЗНАЧЕНИЯ СЕВАСТОПОЛЬ'
        )
        THEN 'Г. СЕВАСТОПОЛЬ'

        WHEN UPPER(okato) IN (
            'Г. МОСКВА',
            'ГОРОД МОСКВА СТОЛИЦА РОССИЙСКОЙ ФЕДЕРАЦИИ ГОРОД ФЕДЕРАЛЬНОГО ЗНАЧЕНИЯ'
        )
        THEN 'Г. МОСКВА'

        -- РФ

        WHEN UPPER(okato) IN (
            'РОССИЙСКАЯ ФЕДЕРАЦИЯ',
            'РОССИЙСКАЯ ФЕДЕРАЦИЯ (КРОМЕ РЕСПУБЛИКИ КРЫМ И ГОРОДА ФЕДЕРАЛЬНОГО ЗНАЧЕНИЯ СЕВАСТОПОЛЯ)'
        )
        THEN NULL -- 'РОССИЙСКАЯ ФЕДЕРАЦИЯ'

        -- Республики

        WHEN UPPER(okato) IN (
            'РЕСПУБЛИКА АДЫГЕЯ',
            -- 'РЕСПУБЛИКА АДЫГЕЯ (АДЫГЕЯ) (ДО 03.06.2014)',
            'РЕСПУБЛИКА АДЫГЕЯ (АДЫГЕЯ)'
        )
        THEN 'РЕСПУБЛИКА АДЫГЕЯ'

        WHEN UPPER(okato) IN (
            'РЕСПУБЛИКА ИНГУШЕТИЯ',
            'РЕСПУБЛИКА ИНГУШЕТИЯ*'
        )
        THEN 'РЕСПУБЛИКА ИНГУШЕТИЯ'

        WHEN UPPER(okato) IN (
            'ЧЕЧЕНСКАЯ РЕСПУБЛИКА',
            'ЧЕЧЕНСКАЯ РЕСПУБЛИКА*'
        )
        THEN 'ЧЕЧЕНСКАЯ РЕСПУБЛИКА'

        WHEN UPPER(okato) IN (
            'РЕСПУБЛИКА ТАТАРСТАН',
            'РЕСПУБЛИКА ТАТАРСТАН (ТАТАРСТАН)'
        )
        THEN 'РЕСПУБЛИКА ТАТАРСТАН'

        WHEN UPPER(okato) IN (
            'ЧУВАШСКАЯ РЕСПУБЛИКА',
            'ЧУВАШСКАЯ РЕСПУБЛИКА - ЧУВАШИЯ'
        )
        THEN 'ЧУВАШСКАЯ РЕСПУБЛИКА'

        WHEN UPPER(okato) IN (
            'РЕСПУБЛИКА СЕВЕРНАЯ ОСЕТИЯ - АЛАНИЯ',
            'РЕСПУБЛИКА СЕВЕРНАЯ ОСЕТИЯ-АЛАНИЯ'
        )
        THEN 'РЕСПУБЛИКА СЕВЕРНАЯ ОСЕТИЯ - АЛАНИЯ'

        WHEN UPPER(okato) IN (
            'РЕСПУБЛИКА КРЫМ'
        )
        THEN 'РЕСПУБЛИКА КРЫМ'

        -- Округа

        WHEN UPPER(okato) IN (
            'НЕНЕЦКИЙ АВТОНОМНЫЙ ОКРУГ',
            'НЕНЕЦКИЙ АВТОНОМНЫЙ ОКРУГ (АРХАНГЕЛЬСКАЯ ОБЛАСТЬ)'
        )
        THEN 'НЕНЕЦКИЙ АВТОНОМНЫЙ ОКРУГ'

        -- Области

        WHEN UPPER(okato) IN (
            'КЕМЕРОВСКАЯ ОБЛАСТЬ',
            'КЕМЕРОВСКАЯ ОБЛАСТЬ - КУЗБАСС'
        )
        THEN 'КЕМЕРОВСКАЯ ОБЛАСТЬ'

        -- Автономные округа, не входящие в первый уровень ОКАТО

        -- 'ЯМАЛО-НЕНЕЦКИЙ АВТОНОМНЫЙ ОКРУГ',
        -- 'ЯМАЛО-НЕНЕЦКИЙ АВТОНОМНЫЙ ОКРУГ (ТЮМЕНСКАЯ ОБЛАСТЬ)',
        -- 'ХАНТЫ-МАНСИЙСКИЙ АВТОНОМНЫЙ ОКРУГ-ЮГРА',
        -- 'ХАНТЫ-МАНСИЙСКИЙ АВТОНОМНЫЙ ОКРУГ - ЮГРА (ТЮМЕНСКАЯ ОБЛАСТЬ)',
        -- 'ТАЙМЫРСКИЙ (ДОЛГАНО-НЕНЕЦКИЙ) АВТОНОМНЫЙ ОКРУГ (КРАСНОЯРСКИЙ КРАЙ)',
        -- 'ЭВЕНКИЙСКИЙ АВТОНОМНЫЙ ОКРУГ (КРАСНОЯРСКИЙ КРАЙ)',
        -- 'КОМИ-ПЕРМЯЦКИЙ ОКРУГ, ВХОДЯЩИЙ В СОСТАВ ПЕРМСКОГО КРАЯ',
        -- 'КОРЯКСКИЙ ОКРУГ, ВХОДЯЩИЙ В СОСТАВ КАМЧАТСКОГО КРАЯ',
        -- 'УСТЬ-ОРДЫНСКИЙ БУРЯТСКИЙ ОКРУГ',

        -- Федеральные округа, не входящие в первый уровень ОКАТО

        -- 'СЕВЕРО-КАВКАЗСКИЙ ФЕДЕРАЛЬНЫЙ ОКРУГ (ДО 03.06.2014)',
        -- 'СЕВЕРО-КАВКАЗСКИЙ ФЕДЕРАЛЬНЫЙ ОКРУГ (С 03.06.2014)',
        -- 'СЕВЕРО-КАВКАЗСКИЙ ФЕДЕРАЛЬНЫЙ ОКРУГ',
        -- 'ЮЖНЫЙ ФЕДЕРАЛЬНЫЙ ОКРУГ  (ДО 03.06.2014)',
        -- 'ЮЖНЫЙ ФЕДЕРАЛЬНЫЙ ОКРУГ  (С 03.06.2014)',
        -- 'ЮЖНЫЙ ФЕДЕРАЛЬНЫЙ  ОКРУГ (С 2010 ГОДА)',
        -- 'ЮЖНЫЙ ФЕДЕРАЛЬНЫЙ ОКРУГ (ПО 2009 ГОД)',
        -- 'ЮЖНЫЙ ФЕДЕРАЛЬНЫЙ ОКРУГ (С 2010 ГОДА)',
        -- 'ЮЖНЫЙ ФЕДЕРАЛЬНЫЙ ОКРУГ (С 29.07.2016)',
        -- 'АГИНСКИЙ БУРЯТСКИЙ ОКРУГ (ЗАБАЙКАЛЬСКИЙ КРАЙ)',
        -- 'ДАЛЬНЕВОСТОЧНЫЙ ФЕДЕРАЛЬНЫЙ ОКРУГ',
        -- 'ПРИВОЛЖСКИЙ ФЕДЕРАЛЬНЫЙ ОКРУГ',
        -- 'СЕВЕРО-ЗАПАДНЫЙ ФЕДЕРАЛЬНЫЙ ОКРУГ',
        -- 'СИБИРСКИЙ ФЕДЕРАЛЬНЫЙ ОКРУГ',
        -- 'УРАЛЬСКИЙ ФЕДЕРАЛЬНЫЙ ОКРУГ',
        -- 'ЦЕНТРАЛЬНЫЙ ФЕДЕРАЛЬНЫЙ ОКРУГ',
        -- 'КРЫМСКИЙ ФЕДЕРАЛЬНЫЙ ОКРУГ',

        -- Районы, не входящие в первый уровень ОКАТО

        -- 'ВОЛГО-ВЯТСКИЙ РАЙОН',
        -- 'ВОСТОЧНО-СИБИРСКИЙ РАЙОН',
        -- 'ДАЛЬНЕВОСТОЧНЫЙ РАЙОН',
        -- 'ЗАПАДНО-СИБИРСКИЙ РАЙОН',
        -- 'ПОВОЛЖСКИЙ РАЙОН',
        -- 'СЕВЕРНЫЙ РАЙОН',
        -- 'СЕВЕРО-ЗАПАДНЫЙ РАЙОН',
        -- 'СЕВЕРО-КАВКАЗСКИЙ РАЙОН',
        -- 'УРАЛЬСКИЙ РАЙОН',
        -- 'ЦЕНТРАЛЬНО-ЧЕРНОЗЕМНЫЙ РАЙОН',
        -- 'ЦЕНТРАЛЬНЫЙ РАЙОН',

        -- Другое

        -- 'ЧЕЧЕНСКАЯ И ИНГУШСКАЯ РЕСПУБЛИКИ',
    END;
END
$$ LANGUAGE plpgsql;


CREATE TABLE normalized.indicator_31556 AS
SELECT
    normalized.normalize_okato("OKATO") AS okato,
    year AS year,
    value * 1 AS value
FROM raw.indicator_31556
WHERE normalized.normalize_okato("OKATO") IS NOT NULL;

-- Общая численность безработных в соответствии с методологией МОТ
-- indicator_33414
CREATE TABLE normalized.unemployed_mot AS
SELECT
    normalized.normalize_okato("OKATO") AS okato,
    year AS year,
    value * 1000 AS value
FROM raw.indicator_33414
WHERE
    vozr = '15-72 лет'
    AND normalized.normalize_okato("OKATO") IS NOT NULL;


CREATE TABLE normalized.indicator_33453 AS
SELECT
    normalized.normalize_okved("OKVED") AS okved,
    normalized.normalize_okato("OKATO") AS okato,
    year AS year,
    value * 1 AS value
FROM raw.indicator_33453
WHERE
    normalized.normalize_okved("OKVED") IS NOT NULL
    AND normalized.normalize_okato("OKATO") IS NOT NULL;


-- Численность выпускников очной формы обучения, продолжающих обучение на следующем уровне
-- indicator_36654, indicator_44266
CREATE TABLE normalized.new_masters_gov AS
    SELECT
        normalized.normalize_okato("OKATO") AS okato,
        year AS year,
        value * 1 AS value
    FROM raw.indicator_36654
    WHERE normalized.normalize_okato("OKATO") IS NOT NULL
UNION ALL
    SELECT
        normalized.normalize_okato("mОКАТО") AS okato,
        year AS year,
        value * 1 AS value
    FROM raw.indicator_44266
    WHERE normalized.normalize_okato("mОКАТО") IS NOT NULL;


-- Предположительная численность населения
-- indicator_36727
CREATE TABLE normalized.population AS
SELECT
    normalized.normalize_okato("OKATO") AS okato,
    year AS year,
    value * 1000 AS value
FROM raw.indicator_36727
WHERE
    normalized.normalize_okato("OKATO") IS NOT NULL
    AND varprogn = 'Средний вариант прогноза';


-- Миграционный прирост трудоспособного возраста
-- indicator_37613
CREATE TABLE normalized.migrants AS
SELECT
    normalized.normalize_okato("OKATO") AS okato,
    year AS year,
    value * 1 AS value
FROM raw.indicator_37613
WHERE normalized.normalize_okato("OKATO") IS NOT NULL;


--
-- indicator_43048_2000, indicator_43048_2005, indicator_43048_2011
CREATE TABLE normalized.indicator_43048 AS
    SELECT
        normalized.normalize_okved("OKVED") AS okved,
        normalized.normalize_okato("OKATO") AS okato,
        year AS year,
        value / 100 AS value
    FROM raw.indicator_43048_2000
    WHERE
        tip = 'Полный круг'
        AND normalized.normalize_okved("OKVED") IS NOT NULL
        AND normalized.normalize_okato("OKATO") IS NOT NULL
UNION ALL
    SELECT
        normalized.normalize_okved("OKVED") AS okved,
        normalized.normalize_okato("OKATO") AS okato,
        year AS year,
        value / 100 AS value
    FROM raw.indicator_43048_2005
    WHERE
        tip = 'Полный круг'
        AND normalized.normalize_okved("OKVED") IS NOT NULL
        AND normalized.normalize_okato("OKATO") IS NOT NULL
UNION ALL
    SELECT
        normalized.normalize_okved("OKVED") AS okved,
        normalized.normalize_okato("OKATO") AS okato,
        year AS year,
        value / 100 AS value
    FROM raw.indicator_43048_2011
    WHERE
        tip = 'Полный круг'
        AND normalized.normalize_okved("OKVED") IS NOT NULL
        AND normalized.normalize_okato("OKATO") IS NOT NULL;


-- Заявленная работодателями потребность в работниках в течении отчетного периода
-- indicator_43141
CREATE TABLE normalized.needed_employees_declared AS
SELECT
    DISTINCT ON (year)
    year AS year,
    value * 1 AS value
FROM raw.indicator_43141;


-- Число выпускников, призванных в ряды Вооруженных Сил
-- indicator_44263
CREATE TABLE normalized.ex_students_conscripted AS
SELECT
    normalized.normalize_okato("mОКАТО") AS okato,
    year AS year,
    value * 1 AS value
FROM raw.indicator_44263
WHERE normalized.normalize_okato("mОКАТО") IS NOT NULL;


-- indicator_44267
-- Принято студентов на обучение ВПО (частные)
CREATE TABLE normalized.new_bachelors_private AS
SELECT
    normalized.normalize_okato("mОКАТО") AS okato,
    year AS year,
    value * 1 AS value
FROM raw.indicator_44267
WHERE normalized.normalize_okato("mОКАТО") IS NOT NULL;


-- indicator_44268
-- Принято студентов на обучение ВПО (государственные)
CREATE TABLE normalized.new_bachelors_gov AS
SELECT
    normalized.normalize_okato("mОКАТО") AS okato,
    year AS year,
    value * 1 AS value
FROM raw.indicator_44268
WHERE normalized.normalize_okato("mОКАТО") IS NOT NULL;


-- Выпуск специалистов государственными ВУЗами
-- indicator_44271
CREATE TABLE normalized.ex_students_gov AS
SELECT
    normalized.normalize_okato("mОКАТО") AS okato,
    year AS year,
    value * 1 AS value
FROM raw.indicator_44271
WHERE normalized.normalize_okato("mОКАТО") IS NOT NULL;


-- Выпуск специалистов частными ВУЗами
-- indicator_44274
CREATE TABLE normalized.ex_students_private AS
SELECT
    normalized.normalize_okato("mОКАТО") AS okato,
    year AS year,
    value * 1 AS value
FROM raw.indicator_44274
WHERE normalized.normalize_okato("mОКАТО") IS NOT NULL;


-- Число пенсионеров
-- indicator_57316
CREATE TABLE normalized.pensioners AS
SELECT
    normalized.normalize_okato("OKATO") AS okato,
    year AS year,
    value * 1000 AS value
FROM raw.indicator_57316
WHERE normalized.normalize_okato("OKATO") IS NOT NULL;


-- Численность трудоустроившихся выпускников
-- indicator_57509
-- NOTE: показатель отброшен, т.к. нет разделения по ОКАТО
CREATE TABLE normalized.indicator_57509 AS
SELECT
    year AS year,
    value * 1000 AS value
FROM raw.indicator_57509
WHERE
    "OKSO" = 'Всего'
    AND "OKATO" = 'Российская Федерация'
    AND progrobras = 'Всего по программам высшего профессионального образования';


-- indicator_57512
-- Численность трудоустроившихся выпускников С СПО
CREATE TABLE normalized.employed_spo AS
SELECT
    year AS year,
    value * 1000 AS value
FROM raw.indicator_57512
WHERE
    specspo = 'Всего'
    AND "OKATO" = 'Российская Федерация'
    AND progrobras = 'Всего по программам среднего профессионального образования';


-- Численность трудоустроившихся выпускников (всего)
-- indicator_57515
CREATE TABLE normalized.ex_students_employed_total AS
SELECT
    normalized.normalize_okato("OKATO") AS okato,
    year AS year,
    value * 1000 AS value
FROM raw.indicator_57515
WHERE
    progrobras = 'Всего'
    AND normalized.normalize_okato("OKATO") IS NOT NULL;


-- Численность трудоустроившихся выпускников (ВПО)
-- indicator_57515
CREATE TABLE normalized.ex_students_employed_vpo AS
SELECT
    normalized.normalize_okato("OKATO") AS okato,
    year AS year,
    SUM(value) * 1000 AS value
FROM raw.indicator_57515
WHERE
    progrobras IN (
        'Программы бакалавриата',
        'Программы высшего образования – специалитет, магистратура',
        'Программы высшего образования (подготовки научно-педагогических кадров в аспирантуре (адъюнктуре), программам ординатуры, ассистентуры-стажировки)'
    )
    AND normalized.normalize_okato("OKATO") IS NOT NULL
GROUP BY normalized.normalize_okato("OKATO"), year
HAVING normalized.normalize_okato("OKATO") IS NOT NULL;


-- Численность трудоустроившихся выпускников (СПО)
-- indicator_57515
CREATE TABLE normalized.ex_students_employed_spo AS
SELECT
    normalized.normalize_okato("OKATO") AS okato,
    year AS year,
    value * 1000 AS value
FROM raw.indicator_57515
WHERE
    progrobras IN (
        'Программы среднего профессионального образования (подготовка квалифицированных рабочих, служащих)',
        'Программы среднего профессионального образования (подготовка специалистов среднего звена)'
    )
    AND normalized.normalize_okato("OKATO") IS NOT NULL;


CREATE TABLE normalized.indicator_57523 AS
SELECT
    year AS year,
    MAX(value) / 100 AS value
FROM raw.indicator_57523
GROUP BY year;


-- Индекс производства (для категории ВЭДов B)
-- -- ТОЛЬКО для ВЭДов:
-- -- - Добыча полезных ископаемых
-- -- - Обрабатывающие производства
-- -- - Обеспечение электрической энергией, газом и паром; кондиционирование воздуха
-- -- - Водоснабжение; водоотведение, организация сбора и утилизации отходов, деятельность по ликвидации загрязнений
-- indicator_57806
CREATE TABLE normalized.production_index_category_b AS
SELECT
    normalized.normalize_okato("OKATO") AS okato,
    year AS year,
    AVG(value) / 100 AS value
FROM raw.indicator_57806
WHERE
    tipisvodov = 'Крупные, средние и малые организации'
    AND normalized.normalize_okved2("OKVED2") IN (
        'ДОБЫЧА ПОЛЕЗНЫХ ИСКОПАЕМЫХ',
        'ОБРАБАТЫВАЮЩИЕ ПРОИЗВОДСТВА',
        'ОБЕСПЕЧЕНИЕ ЭЛЕКТРИЧЕСКОЙ ЭНЕРГИЕЙ, ГАЗОМ И ПАРОМ; КОНДИЦИОНИРОВАНИЕ ВОЗДУХА',
        'ВОДОСНАБЖЕНИЕ; ВОДООТВЕДЕНИЕ, ОРГАНИЗАЦИЯ СБОРА И УТИЛИЗАЦИИ ОТХОДОВ, ДЕЯТЕЛЬНОСТЬ ПО ЛИКВИДАЦИИ ЗАГРЯЗНЕНИЙ'
    )
    AND normalized.normalize_okato("OKATO") IS NOT NULL
GROUP BY normalized.normalize_okato("OKATO"), year
HAVING normalized.normalize_okato("OKATO") IS NOT NULL;


-- Инвестиции в основной капитал (для 3-й категории ВЭДов)
-- ТОЛЬКО по ВЭДам:
-- - Деятельность профессиональная, научная и техническая
-- - Деятельность административная и сопутствующие дополнительные  услуги
-- - Государственное управление и обеспечение военной безопасности; социальное обеспечение
-- - Образование
-- - Деятельность в области здравоохранения и социальных услуг
-- - Деятельность в области культуры, спорта, организации досуга и развлечений
-- - Деятельность домашних хозяйств как работодателей; недифференцированная деятельность частных домашних хозяйств
-- - Деятельность экстерриториальных организаций и органов
-- indicator_40534, indicator_58090
CREATE TABLE normalized.investments_category_c AS
    SELECT
        normalized.normalize_okato(okato) AS okato,
        year AS year,
        SUM(value) * 1000000 AS value
    FROM raw.indicator_40534
    WHERE
        normalized.normalize_okved(okved) IN (
            'РАЗДЕЛ L ГОСУДАРСТВЕННОЕ УПРАВЛЕНИЕ И ОБЕСПЕЧЕНИЕ ВОЕННОЙ БЕЗОПАСНОСТИ; СОЦИАЛЬНОЕ СТРАХОВАНИЕ',
            'РАЗДЕЛ M ОБРАЗОВАНИЕ',
            'РАЗДЕЛ N ЗДРАВООХРАНЕНИЕ И ПРЕДОСТАВЛЕНИЕ СОЦИАЛЬНЫХ  УСЛУГ',
            'РАЗДЕЛ O ПРЕДОСТАВЛЕНИЕ ПРОЧИХ КОММУНАЛЬНЫХ,  СОЦИАЛЬНЫХ И ПЕРСОНАЛЬНЫХ УСЛУГ',
            'РАЗДЕЛ P ДЕЯТЕЛЬНОСТЬ ДОМАШНИХ ХОЗЯЙСТВ',
            'РАЗДЕЛ Q ДЕЯТЕЛЬНОСТЬ ЭКСТЕРРИТОРИАЛЬНЫХ ОРГАНИЗАЦИЙ'
        )
        AND normalized.normalize_okato(okato) IS NOT NULL
    GROUP BY normalized.normalize_okato(okato), year
    HAVING normalized.normalize_okato(okato) IS NOT NULL
UNION ALL
    SELECT
        normalized.normalize_okato("OKATO") AS okato,
        year AS year,
        SUM(value) * 1000 AS value
    FROM raw.indicator_58090
    WHERE
        tipisvodov = 'Крупные и средние организации и организации с численностью работников до 15 человек, не являющиеся субъектами малого предпринимательства'
        AND normalized.normalize_okved2("OKVED2") IN (
            'ДЕЯТЕЛЬНОСТЬ ПРОФЕССИОНАЛЬНАЯ, НАУЧНАЯ И ТЕХНИЧЕСКАЯ',
            'ДЕЯТЕЛЬНОСТЬ АДМИНИСТРАТИВНАЯ И СОПУТСТВУЮЩИЕ ДОПОЛНИТЕЛЬНЫЕ УСЛУГИ',
            'ГОСУДАРСТВЕННОЕ УПРАВЛЕНИЕ И ОБЕСПЕЧЕНИЕ ВОЕННОЙ БЕЗОПАСНОСТИ; СОЦИАЛЬНОЕ ОБЕСПЕЧЕНИЕ',
            'ОБРАЗОВАНИЕ',
            'ДЕЯТЕЛЬНОСТЬ В ОБЛАСТИ ЗДРАВООХРАНЕНИЯ И СОЦИАЛЬНЫХ УСЛУГ',
            'ДЕЯТЕЛЬНОСТЬ В ОБЛАСТИ КУЛЬТУРЫ, СПОРТА, ОРГАНИЗАЦИИ ДОСУГА И РАЗВЛЕЧЕНИЙ',
            'ДЕЯТЕЛЬНОСТЬ ДОМАШНИХ ХОЗЯЙСТВ КАК РАБОТОДАТЕЛЕЙ; НЕДИФФЕРЕНЦИРОВАННАЯ ДЕЯТЕЛЬНОСТЬ ЧАСТНЫХ ДОМАШНИХ ХОЗЯЙСТВ ПО ПРОИЗВОДСТВУ ТОВАРОВ И ОКАЗАНИЮ УСЛУГ ДЛЯ СОБСТВЕННОГО ПОТРЕБЛЕНИЯ',
            'ДЕЯТЕЛЬНОСТЬ ЭКСТЕРРИТОРИАЛЬНЫХ ОРГАНИЗАЦИЙ И ОРГАНОВ'
        )
        AND normalized.normalize_okato("OKATO") IS NOT NULL
    GROUP BY normalized.normalize_okato("OKATO"), year
    HAVING normalized.normalize_okato("OKATO") IS NOT NULL;


-- Численность принятых работников (по ОКАТО)
-- indicator_33465, indicator_58145
CREATE TABLE normalized.employed AS
    SELECT
        normalized.normalize_okato("OKATO") AS okato,
        year AS year,
        SUM(value) * 1 AS value
    FROM raw.indicator_33465
    WHERE
        normalized.normalize_okved("OKVED") IS NOT NULL
        AND normalized.normalize_okved("OKVED") <> 'ВСЕГО'
        AND normalized.normalize_okato("OKATO") IS NOT NULL
    GROUP BY normalized.normalize_okato("OKATO"), year
    HAVING normalized.normalize_okato("OKATO") IS NOT NULL
UNION ALL
    SELECT
        normalized.normalize_okato("OKATO") AS okato,
        year AS year,
        SUM(value) * 1 AS value
    FROM raw.indicator_58145_1
    WHERE
        normalized.normalize_okved2("OKVED2") IS NOT NULL
        AND normalized.normalize_okved2("OKVED2") <> 'Всего по обследуемым видам экономической деятельности'
        AND normalized.normalize_okato("OKATO") IS NOT NULL
    GROUP BY normalized.normalize_okato("OKATO"), year
    HAVING normalized.normalize_okato("OKATO") IS NOT NULL
UNION ALL
    SELECT
        normalized.normalize_okato("OKATO") AS okato,
        year AS year,
        SUM(value) * 1 AS value
    FROM raw.indicator_58145_2
    WHERE
        normalized.normalize_okved2("OKVED2") IS NOT NULL
        AND normalized.normalize_okved2("OKVED2") <> 'Всего по обследуемым видам экономической деятельности'
        AND normalized.normalize_okato("OKATO") IS NOT NULL
    GROUP BY normalized.normalize_okato("OKATO"), year
    HAVING normalized.normalize_okato("OKATO") IS NOT NULL
UNION ALL
    SELECT
        normalized.normalize_okato("OKATO") AS okato,
        year AS year,
        SUM(value) * 1 AS value
    FROM raw.indicator_58145_3
    WHERE
        normalized.normalize_okved2("OKVED2") IS NOT NULL
        AND normalized.normalize_okved2("OKVED2") <> 'Всего по обследуемым видам экономической деятельности'
        AND normalized.normalize_okato("OKATO") IS NOT NULL
    GROUP BY normalized.normalize_okato("OKATO"), year
    HAVING normalized.normalize_okato("OKATO") IS NOT NULL;


CREATE TABLE normalized.indicator_58148 AS
    SELECT
        normalized.normalize_okved2("OKVED2") AS okved,
        normalized.normalize_okato("OKATO") AS okato,
        year AS year,
        value * 1 AS value
    FROM raw.indicator_58148_1
    WHERE
        normalized.normalize_okved2("OKVED2") IS NOT NULL
        AND normalized.normalize_okato("OKATO") IS NOT NULL
UNION ALL
    SELECT
        normalized.normalize_okved2("OKVED2") AS okved,
        normalized.normalize_okato("OKATO") AS okato,
        year AS year,
        value * 1 AS value
    FROM raw.indicator_58148_2
    WHERE
        normalized.normalize_okved2("OKVED2") IS NOT NULL
        AND normalized.normalize_okato("OKATO") IS NOT NULL
UNION ALL
    SELECT
        normalized.normalize_okved2("OKVED2") AS okved,
        normalized.normalize_okato("OKATO") AS okato,
        year AS year,
        value * 1 AS value
    FROM raw.indicator_58148_3
    WHERE
        normalized.normalize_okved2("OKVED2") IS NOT NULL
        AND normalized.normalize_okato("OKATO") IS NOT NULL;


-- Численность лиц, обученных в организациях по программам профессиональной переподготовки
-- indicator_58264
CREATE TABLE normalized.retraining AS
SELECT
    normalized.normalize_okato("mОКАТО") AS okato,
    year AS year,
    value * 1 AS value
FROM raw.indicator_58264
WHERE normalized.normalize_okato("mОКАТО") IS NOT NULL;


-- Число "выбывших", трудоспособного населения по годам и ОКАТО
-- indicator_58614
CREATE TABLE normalized.retriement_employable AS
SELECT
    normalized.normalize_okato("OKATO") AS okato,
    year AS year,
    value * 1 AS value
FROM raw.indicator_58614
WHERE
    vozr = 'Трудоспособное'
    AND normalized.normalize_okato("OKATO") IS NOT NULL;


-- Среднесписочная численность работников (для всех ведов в разрезе ОКАТО)
-- indicator_43007, indicator_58699
CREATE TABLE normalized.average_employees AS
    SELECT
        normalized.normalize_okato(okato) AS okato,
        year AS year,
        SUM(value) * 1 AS value
    FROM raw.indicator_43007
    WHERE
        normalized.normalize_okved(okved) IS NOT NULL
        AND normalized.normalize_okato(okato) IS NOT NULL
    GROUP BY normalized.normalize_okato(okato), year
    HAVING normalized.normalize_okato(okato) IS NOT NULL
UNION ALL
    SELECT
        normalized.normalize_okato("OKATO") AS okato,
        year AS year,
        SUM(value) * 1 AS value
    FROM raw.indicator_58699
    WHERE
        normalized.normalize_okved2("OKVED2") IS NOT NULL
        AND normalized.normalize_okato("OKATO") IS NOT NULL
    GROUP BY normalized.normalize_okato("OKATO"), year
    HAVING normalized.normalize_okato("OKATO") IS NOT NULL;


-- Среднесписочная численность работников (для категории ВЭДов A)
-- ТОЛЬКО для ВЭДов:
-- - Сельское, лесное хозяйство, охота,рыболовство и рыбоводство
-- - Строительство
-- - Торговля оптовая и розничная; ремонт автотранспортных средств и мотоциклов
-- - Транспортировка и хранение
-- - Деятельность в области информации и связи
-- - Деятельность гостиниц и предприятий общественного питания
-- - Деятельность финансовая и страховая
-- - Деятельность по операциям с недвижимым имуществом
-- - Предоставление прочих видов услуг
-- indicator_43007, indicator_58699
CREATE TABLE normalized.average_employees_category_a AS
    SELECT
        normalized.normalize_okato(okato) AS okato,
        year AS year,
        SUM(value) * 1 AS value
    FROM raw.indicator_43007
    WHERE
        normalized.normalize_okved(okved) IN (
            'РАЗДЕЛ A СЕЛЬСКОЕ ХОЗЯЙСТВО, ОХОТА И ЛЕСНОЕ ХОЗЯЙСТВО',
            'РАЗДЕЛ F СТРОИТЕЛЬСТВО',
            'РАЗДЕЛ G ОПТОВАЯ И РОЗНИЧНАЯ ТОРГОВЛЯ; РЕМОНТ АВТОТРАНСПОРТНЫХ СРЕДСТВ, МОТОЦИКЛОВ, БЫТОВЫХ ИЗДЕЛИЙ И ПРЕДМЕТОВ ЛИЧНОГО ПОЛЬЗОВАНИЯ',
            'РАЗДЕЛ I ТРАНСПОРТ И СВЯЗЬ',
            'РАЗДЕЛ H ГОСТИНИЦЫ И РЕСТОРАНЫ',
            'РАЗДЕЛ J ФИНАНСОВАЯ ДЕЯТЕЛЬНОСТЬ',
            'РАЗДЕЛ K ОПЕРАЦИИ С НЕДВИЖИМЫМ ИМУЩЕСТВОМ, АРЕНДА И ПРЕДОСТАВЛЕНИЕ УСЛУГ'
        )
        AND normalized.normalize_okato(okato) IS NOT NULL
    GROUP BY normalized.normalize_okato(okato), year
    HAVING normalized.normalize_okato(okato) IS NOT NULL
UNION ALL
    SELECT
        normalized.normalize_okato("OKATO") AS okato,
        year AS year,
        SUM(value) * 1 AS value
    FROM raw.indicator_58699
    WHERE
        normalized.normalize_okved2("OKVED2") IN (
            'СЕЛЬСКОЕ, ЛЕСНОЕ ХОЗЯЙСТВО, ОХОТА, РЫБОЛОВСТВО И РЫБОВОДСТВО',
            'СТРОИТЕЛЬСТВО',
            'ТОРГОВЛЯ ОПТОВАЯ И РОЗНИЧНАЯ; РЕМОНТ АВТОТРАНСПОРТНЫХ СРЕДСТВ И МОТОЦИКЛОВ',
            'ТРАНСПОРТИРОВКА И ХРАНЕНИЕ',
            'ДЕЯТЕЛЬНОСТЬ В ОБЛАСТИ ИНФОРМАЦИИ И СВЯЗИ',
            'ДЕЯТЕЛЬНОСТЬ ФИНАНСОВАЯ И СТРАХОВАЯ',
            'ДЕЯТЕЛЬНОСТЬ ГОСТИНИЦ И ПРЕДПРИЯТИЙ ОБЩЕСТВЕННОГО ПИТАНИЯ',
            'ДЕЯТЕЛЬНОСТЬ ПО ОПЕРАЦИЯМ С НЕДВИЖИМЫМ ИМУЩЕСТВОМ',
            'ПРЕДОСТАВЛЕНИЕ ПРОЧИХ ВИДОВ УСЛУГ'
        )
        AND normalized.normalize_okato("OKATO") IS NOT NULL
    GROUP BY normalized.normalize_okato("OKATO"), year
    HAVING normalized.normalize_okato("OKATO") IS NOT NULL;


-- Среднесписочная численность работников (для категории ВЭДов B)
-- ТОЛЬКО для ВЭДов:
-- - Добыча полезных ископаемых
-- - Обрабатывающие производства
-- - Обеспечение электрической энергией, газом и паром; кондиционирование воздуха
-- - Водоснабжение; водоотведение, организация сбора и утилизации отходов, деятельность по ликвидации загрязнений
-- indicator_43007, indicator_58699
CREATE TABLE normalized.average_employees_category_b AS
    SELECT
        normalized.normalize_okato(okato) AS okato,
        year AS year,
        SUM(value) * 1 AS value
    FROM raw.indicator_43007
    WHERE
        normalized.normalize_okved(okved) IN (
            'РАЗДЕЛ C ДОБЫЧА ПОЛЕЗНЫХ ИСКОПАЕМЫХ',
            'РАЗДЕЛ D ОБРАБАТЫВАЮЩИЕ ПРОИЗВОДСТВА',
            'РАЗДЕЛ E ПРОИЗВОДСТВО И РАСПРЕДЕЛЕНИЕ ЭЛЕКТРОЭНЕРГИИ, ГАЗА И ВОДЫ'
        )
        AND normalized.normalize_okato(okato) IS NOT NULL
    GROUP BY normalized.normalize_okato(okato), year
    HAVING normalized.normalize_okato(okato) IS NOT NULL
UNION ALL
    SELECT
        normalized.normalize_okato("OKATO") AS okato,
        year AS year,
        SUM(value) * 1 AS value
    FROM raw.indicator_58699
    WHERE
        normalized.normalize_okved2("OKVED2") IN (
            'ДОБЫЧА ПОЛЕЗНЫХ ИСКОПАЕМЫХ',
            'ОБРАБАТЫВАЮЩИЕ ПРОИЗВОДСТВА',
            'ОБЕСПЕЧЕНИЕ ЭЛЕКТРИЧЕСКОЙ ЭНЕРГИЕЙ, ГАЗОМ И ПАРОМ; КОНДИЦИОНИРОВАНИЕ ВОЗДУХА',
            'ВОДОСНАБЖЕНИЕ; ВОДООТВЕДЕНИЕ, ОРГАНИЗАЦИЯ СБОРА И УТИЛИЗАЦИИ ОТХОДОВ, ДЕЯТЕЛЬНОСТЬ ПО ЛИКВИДАЦИИ ЗАГРЯЗНЕНИЙ'
        )
        AND normalized.normalize_okato("OKATO") IS NOT NULL
    GROUP BY normalized.normalize_okato("OKATO"), year
    HAVING normalized.normalize_okato("OKATO") IS NOT NULL;


-- indicator_58775
-- Число умерших трудоспособного возраста по годам и ОКАТО
CREATE TABLE normalized.decease_employable AS
SELECT
    normalized.normalize_okato("OKATO") AS okato,
    year AS year,
    SUM(value) * 1 as value
FROM raw.indicator_58775
WHERE
    (
        "S_GRUP_2" = 'Мужчины'
        AND vozr IN ('20-24 лет', '20-24', '25-29 лет', '30-34 лет', '35-39 лет', '40-44 лет', '45-49 лет', '50-54 лет', '55-59 лет', '60-64 лет')
    ) OR (
        "S_GRUP_2" = 'Женщины'
        AND vozr IN ('20-24 лет', '20-24', '25-29 лет', '30-34 лет', '35-39 лет', '40-44 лет', '45-49 лет', '50-54 лет', '55-59 лет')
    )
GROUP BY normalized.normalize_okato("OKATO"), year
HAVING normalized.normalize_okato("OKATO") IS NOT NULL;


-- Число умерших старше трудоспособного возраста по годам и ОКАТО
CREATE TABLE normalized.decease_unemployable AS
SELECT
    normalized.normalize_okato("OKATO") AS okato,
    year AS year,
    SUM(value) * 1 as value
FROM raw.indicator_58775
WHERE
    (
        "S_GRUP_2" = 'Мужчины'
        AND vozr IN ('70 лет и старше', '65-69 лет')
    ) OR (
        "S_GRUP_2" = 'Женщины'
        AND vozr IN ('70 лет и старше', '65-69 лет', '60-64 лет')
    )
GROUP BY normalized.normalize_okato("OKATO"), year
HAVING normalized.normalize_okato("OKATO") IS NOT NULL;


-- Потребность в работниках для замещения вакантных рабочих мест
-- indicator_59086, indicator_57886, indicator_40608
CREATE TABLE normalized.needed_employees_to_fill AS
SELECT
    normalized.normalize_okved2("OKVED2") AS okved,
    normalized.normalize_okato("OKATO") AS okato,
    year AS year,
    value * 1 AS value
FROM raw.indicator_59086
WHERE
    normalized.normalize_okved2("OKVED2") IS NOT NULL
    AND normalized.normalize_okato("OKATO") IS NOT NULL
UNION ALL
SELECT
    normalized.normalize_okved("OKVED") AS okved,
    normalized.normalize_okato("OKATO") AS okato,
    year AS year,
    value * 1 AS value
FROM raw.indicator_57886
WHERE
    normalized.normalize_okved("OKVED") IS NOT NULL
    AND normalized.normalize_okato("OKATO") IS NOT NULL
    AND okz2014 = 'Всего специалистов'
UNION ALL
SELECT
    normalized.normalize_okved(okved) AS okved,
    normalized.normalize_okato(okato) AS okato,
    year AS year,
    value * 1 AS value
FROM raw.indicator_40608
WHERE
    profgrup = 'Всего специалистов'
    AND normalized.normalize_okved(okved) IS NOT NULL
    AND normalized.normalize_okato(okato) IS NOT NULL;


-- Валовой региональный продукт в основных ценах
-- ТОЛЬКО для ВЭДов:
-- - Сельское, лесное хозяйство, охота,рыболовство и рыбоводство
-- - Строительство
-- - Торговля оптовая и розничная; ремонт автотранспортных средств и мотоциклов
-- - Транспортировка и хранение
-- - Деятельность в области информации и связи
-- - Деятельность гостиниц и предприятий общественного питания
-- - Деятельность финансовая и страховая
-- - Деятельность по операциям с недвижимым имуществом
-- - Предоставление прочих видов услуг
-- indicator_33379, indicator_59448
CREATE TABLE normalized.vrp_category_a AS
    SELECT
        normalized.normalize_okato("OKATO") AS okato,
        year AS year,
        SUM(value) * 1000 AS value
    FROM raw.indicator_33379
    WHERE
        normalized.normalize_okved("OKVED") IN (
            'РАЗДЕЛ A СЕЛЬСКОЕ ХОЗЯЙСТВО, ОХОТА И ЛЕСНОЕ ХОЗЯЙСТВО',
            'РАЗДЕЛ F СТРОИТЕЛЬСТВО',
            'РАЗДЕЛ G ОПТОВАЯ И РОЗНИЧНАЯ ТОРГОВЛЯ; РЕМОНТ АВТОТРАНСПОРТНЫХ СРЕДСТВ, МОТОЦИКЛОВ, БЫТОВЫХ ИЗДЕЛИЙ И ПРЕДМЕТОВ ЛИЧНОГО ПОЛЬЗОВАНИЯ',
            'РАЗДЕЛ I ТРАНСПОРТ И СВЯЗЬ',
            'РАЗДЕЛ H ГОСТИНИЦЫ И РЕСТОРАНЫ',
            'РАЗДЕЛ J ФИНАНСОВАЯ ДЕЯТЕЛЬНОСТЬ',
            'РАЗДЕЛ K ОПЕРАЦИИ С НЕДВИЖИМЫМ ИМУЩЕСТВОМ, АРЕНДА И ПРЕДОСТАВЛЕНИЕ УСЛУГ'
        )
        AND normalized.normalize_okato("OKATO") IS NOT NULL
    GROUP BY normalized.normalize_okato("OKATO"), year
    HAVING normalized.normalize_okato("OKATO") IS NOT NULL
UNION ALL
    SELECT
        normalized.normalize_okato("OKATO") AS okato,
        year AS year,
        SUM(value) * 1000 AS value
    FROM raw.indicator_59448
    WHERE
        normalized.normalize_okved2("OKVED2") IN (
            'СЕЛЬСКОЕ, ЛЕСНОЕ ХОЗЯЙСТВО, ОХОТА, РЫБОЛОВСТВО И РЫБОВОДСТВО',
            'СТРОИТЕЛЬСТВО',
            'ТОРГОВЛЯ ОПТОВАЯ И РОЗНИЧНАЯ; РЕМОНТ АВТОТРАНСПОРТНЫХ СРЕДСТВ И МОТОЦИКЛОВ',
            'ТРАНСПОРТИРОВКА И ХРАНЕНИЕ',
            'ДЕЯТЕЛЬНОСТЬ В ОБЛАСТИ ИНФОРМАЦИИ И СВЯЗИ',
            'ДЕЯТЕЛЬНОСТЬ ФИНАНСОВАЯ И СТРАХОВАЯ',
            'ДЕЯТЕЛЬНОСТЬ ГОСТИНИЦ И ПРЕДПРИЯТИЙ ОБЩЕСТВЕННОГО ПИТАНИЯ',
            'ДЕЯТЕЛЬНОСТЬ ПО ОПЕРАЦИЯМ С НЕДВИЖИМЫМ ИМУЩЕСТВОМ',
            'ПРЕДОСТАВЛЕНИЕ ПРОЧИХ ВИДОВ УСЛУГ'
        )
        AND normalized.normalize_okato("OKATO") IS NOT NULL
    GROUP BY normalized.normalize_okato("OKATO"), year
    HAVING normalized.normalize_okato("OKATO") IS NOT NULL;