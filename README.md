# diploma-emiss-database

Набор утилит для инициализации и наполнения базы данных PostgreSQL данными из источника ЕМИСС ([https://fedstat.ru/](https://fedstat.ru/)) для дипломной работы.

## Требуемые инструменты

- PostgreSQL
- Python 3.x
- Gecko WebDriver

## Порядок инициализации БД

1. Установить PostgreSQL
1. Создать пользователя: `% createuser "<username>"`
1. Создать базу данных для ЕМИСС: `% createdb "<database>"`
1. Перейти в psql: `% psql "<database>"`. *Дальнейшие команды выполнять в оболочке `psql`*
1. Установить пароль для созданного пользователя: `# alter user "<username>" with encrypted password '<password>';`
1. Создать схему `raw`: `# create schema raw;`
1. Выдать доступ к БД новому пользователю: `# grant all privileges on database "<database>" to "<username>";`
1. Выдать доступ к схеме `raw`: `# grant all privileges on schema raw to "<username>";`

*Команды, начинающиеся с `%` следует выполнять с правами пользователя postgres. Для этого можно выполнять их как `$ sudo -u postgres <command>`, без приглашений комнадной строки (начальных символов `$`, `#` и `%`)*

*Вместо `<username>`, `<password>` и `<database>` следует подставить имя пользователя, пароль и имя базы данных для БД ЕМИСС соответственно, без угловых скобок*

## Запуск ETL

1. Установить зависимости: `# pip3 install -r requirements.txt`
1. Установить пароль, через переменную среды `EMISS_DB_PASSWORD`: `$ export EMISS_DB_PASSWORD=<password>`
1. Поместить скачанные вручную SDMX файлы в директорию `local-indicators` (в текущей директории)
1. Запустить ETL: `$ python3 ./download_emiss_to_database.py`
